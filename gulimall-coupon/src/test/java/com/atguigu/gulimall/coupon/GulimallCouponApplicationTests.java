package com.atguigu.gulimall.coupon;

import com.atguigu.gulimall.coupon.entity.CouponEntity;
import com.atguigu.gulimall.coupon.service.CouponService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class GulimallCouponApplicationTests {

    @Autowired
    private CouponService couponService;


    @Test
    void contextLoads() {

        List<CouponEntity> list = couponService.list(new QueryWrapper<CouponEntity>().eq("id",1L));
        list.forEach(node->{
            System.out.println(node);
        });
    }

}
