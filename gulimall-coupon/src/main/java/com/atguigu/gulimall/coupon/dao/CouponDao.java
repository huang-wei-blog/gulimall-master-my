package com.atguigu.gulimall.coupon.dao;

import com.atguigu.gulimall.coupon.entity.CouponEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 优惠券信息
 * 
 * @author huangwei
 * @email 1142488172@qq.com
 * @date 2022-03-11 15:46:00
 */
@Mapper
public interface CouponDao extends BaseMapper<CouponEntity> {
	
}
