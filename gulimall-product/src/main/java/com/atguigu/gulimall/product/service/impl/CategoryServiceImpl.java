package com.atguigu.gulimall.product.service.impl;

import com.alibaba.fastjson.JSON;
import com.atguigu.common.constant.RedisConstant;
import com.atguigu.gulimall.product.vo.CategoryEntityVo;
import com.atguigu.gulimall.product.vo.Catelog2Vo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.redisson.api.RLock;
import org.redisson.api.RReadWriteLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.common.utils.Query;

import com.atguigu.gulimall.product.dao.CategoryDao;
import com.atguigu.gulimall.product.entity.CategoryEntity;
import com.atguigu.gulimall.product.service.CategoryService;
import org.springframework.transaction.annotation.Transactional;



@Service("categoryService")
@Slf4j
public class CategoryServiceImpl extends ServiceImpl<CategoryDao, CategoryEntity> implements CategoryService {

    @Autowired
    StringRedisTemplate redisTemplate;

    @Autowired
    private RedissonClient redissonClient;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<CategoryEntity> page = this.page(
                new Query<CategoryEntity>().getPage(params),
                new QueryWrapper<CategoryEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public List<CategoryEntity> listWithTree() {

        //1、查询出所有分类
        List<CategoryEntity> entities = super.baseMapper.selectList(null);

        //2、组装成父子的树形结构

        //2.1)、找到所有一级分类
        List<CategoryEntity> levelMenus = entities.stream()
                .filter(e -> e.getParentCid() == 0)
                .map((menu) -> {
                    menu.setChildren(getChildrens(menu, entities));
                    return menu;
                })
                .sorted((menu, menu2) -> {
                    return (menu.getSort() == null ? 0 : menu.getSort()) - (menu2.getSort() == null ? 0 : menu2.getSort());
                })
                .collect(Collectors.toList());

        return levelMenus;
    }

    @Override
    public void removeMenuByIds(List<Long> asList) {
        //逻辑删除
        baseMapper.deleteBatchIds(asList);

    }


    //递归查找所有菜单的子菜单
    private List<CategoryEntity> getChildrens(CategoryEntity root, List<CategoryEntity> all) {

        List<CategoryEntity> children = all.stream().filter(categoryEntity -> {
            return categoryEntity.getParentCid().equals(root.getCatId());
        }).map(categoryEntity -> {
            //1、找到子菜单(递归)
            categoryEntity.setChildren(getChildrens(categoryEntity, all));
            return categoryEntity;
        }).sorted((menu, menu2) -> {
            //2、菜单的排序
            return (menu.getSort() == null ? 0 : menu.getSort()) - (menu2.getSort() == null ? 0 : menu2.getSort());
        }).collect(Collectors.toList());

        return children;

    }

    //[2,29,20]
    @Override
    public Long[] findCatelogPath(Long catelogId) {

        List<Long> paths = new ArrayList<>();

        //递归查询是否还有父节点
        List<Long> parentPath = findParentPath(catelogId, paths);

        //进行一个逆序排列
        Collections.reverse(parentPath);

        return (Long[]) parentPath.toArray(new Long[parentPath.size()]);
    }


    /**
     * 级联更新所有关联的数据
     *
     * @CacheEvict:失效模式
     * @CachePut:双写模式，需要有返回值
     * 1、同时进行多种缓存操作：@Caching
     * 2、指定删除某个分区下的所有数据 @CacheEvict(value = "category",allEntries = true)
     * 3、存储同一类型的数据，都可以指定为同一分区
     * @param category
     */
    // @Caching(evict = {
    //         @CacheEvict(value = "category",key = "'getLevel1Categorys'"),
    //         @CacheEvict(value = "category",key = "'getCatalogJson'")
    // })
    @CacheEvict(value = "category",allEntries = true)       //删除某个分区下的所有数据
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateCascade(CategoryEntity category) {


        try {
            this.baseMapper.updateById(category);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        }

        //同时修改缓存中的数据
        //删除缓存,等待下一次主动查询进行更新
    }


    /**
     * 每一个需要缓存的数据我们都来指定要放到那个名字的缓存。【缓存的分区(按照业务类型分)】
     * 代表当前方法的结果需要缓存，如果缓存中有，方法都不用调用，如果缓存中没有，会调用方法。最后将方法的结果放入缓存
     * 默认行为
     * 如果缓存中有，方法不再调用
     * key是默认生成的:缓存的名字::SimpleKey::[](自动生成key值)
     * 缓存的value值，默认使用jdk序列化机制，将序列化的数据存到redis中
     * 默认时间是 -1：
     * <p>
     * 自定义操作：key的生成
     * 指定生成缓存的key：key属性指定，接收一个Spel
     * 指定缓存的数据的存活时间:配置文档中修改存活时间
     * 将数据保存为json格式
     * <p>
     * <p>
     * 4、Spring-Cache的不足之处：
     * 1）、读模式
     * 缓存穿透：查询一个null数据。解决方案：缓存空数据
     * 缓存击穿：大量并发进来同时查询一个正好过期的数据。解决方案：加锁 ? 默认是无加锁的;使用sync = true来解决击穿问题
     * 缓存雪崩：大量的key同时过期。解决：加随机时间。加上过期时间
     * 2)、写模式：（缓存与数据库一致）
     * 1）、读写加锁。
     * 2）、引入Canal,感知到MySQL的更新去更新Redis
     * 3）、读多写多，直接去数据库查询就行
     * <p>
     * 总结：
     * 常规数据（读多写少，即时性，一致性要求不高的数据，完全可以使用Spring-Cache）：写模式(只要缓存的数据有过期时间就足够了)
     * 特殊数据：特殊设计
     * <p>
     * 原理：
     * CacheManager(RedisCacheManager)->Cache(RedisCache)->Cache负责缓存的读写
     *
     * @return
     */
    @Cacheable(value = {"category"}, key = "#root.method.name", sync = true)
    @Override
    public List<CategoryEntityVo> getCatalogJson() {
        log.info("开始获取缓存数据");
        long l = System.currentTimeMillis();
        List<CategoryEntityVo> categorysFromDBWithRedisLock = getCategorysFromDB();
        log.info("业务执行完毕，消耗时间：{}",System.currentTimeMillis() - l);
        return categorysFromDBWithRedisLock;
    }

    @Override
    @Cacheable(value = {"category"},key = "#root.method.name",sync = true)
    public List<CategoryEntity> getLevel1Categorys() {
        System.out.println("getLevel1Categorys........");
        long l = System.currentTimeMillis();
        List<CategoryEntity> categoryEntities = this.baseMapper.selectList(
                new QueryWrapper<CategoryEntity>().eq("parent_cid", 0));
        System.out.println("消耗时间："+ (System.currentTimeMillis() - l));
        return categoryEntities;

    }

    @Override
    @Cacheable(value = "category",key = "#root.methodName")
    public Map<String, List<Catelog2Vo>> getCatalog2Json() {
        log.info("查询了数据库");
        //将数据库的多次查询变为一次
        List<CategoryEntity> selectList = this.baseMapper.selectList(null);

        //1、查出所有分类
        //1、1）查出所有一级分类
        List<CategoryEntity> level1Categorys = getParent_cid(selectList, 0L);

        //封装数据
        Map<String, List<Catelog2Vo>> parentCid = level1Categorys.stream().collect(Collectors.toMap(k -> k.getCatId().toString(), v -> {
            //1、每一个的一级分类,查到这个一级分类的二级分类
            List<CategoryEntity> categoryEntities = getParent_cid(selectList, v.getCatId());

            //2、封装上面的结果
            List<Catelog2Vo> catelog2Vos = null;
            if (categoryEntities != null) {
                catelog2Vos = categoryEntities.stream().map(l2 -> {
                    Catelog2Vo catelog2Vo = new Catelog2Vo(v.getCatId().toString(), null, l2.getCatId().toString(), l2.getName().toString());

                    //1、找当前二级分类的三级分类封装成vo
                    List<CategoryEntity> level3Catelog = getParent_cid(selectList, l2.getCatId());

                    if (level3Catelog != null) {
                        List<Catelog2Vo.Category3Vo> category3Vos = level3Catelog.stream().map(l3 -> {
                            //2、封装成指定格式
                            Catelog2Vo.Category3Vo category3Vo = new Catelog2Vo.Category3Vo(l2.getCatId().toString(), l3.getCatId().toString(), l3.getName());

                            return category3Vo;
                        }).collect(Collectors.toList());
                        catelog2Vo.setCatalog3List(category3Vos);
                    }

                    return catelog2Vo;
                }).collect(Collectors.toList());
            }

            return catelog2Vos;
        }));

        return parentCid;
    }

    private List<CategoryEntity> getParent_cid(List<CategoryEntity> selectList,Long parentCid) {
        List<CategoryEntity> categoryEntities = selectList.stream().filter(item -> item.getParentCid().equals(parentCid)).collect(Collectors.toList());
        return categoryEntities;
        // return this.baseMapper.selectList(
        //         new QueryWrapper<CategoryEntity>().eq("parent_cid", parentCid));
    }

    public List<CategoryEntityVo> getCatalogJson2() {
        log.info("开始获取缓存数据");
        long l = System.currentTimeMillis();

        String categoryJson = redisTemplate.opsForValue().get(RedisConstant.redis_category_json);
        List<CategoryEntityVo> categoryEntityVos = new ArrayList<>();
        if (StringUtils.isNotBlank(categoryJson)) {
            categoryEntityVos = JSON.parseArray(categoryJson, CategoryEntityVo.class);
            log.info("业务执行完毕，消耗时间：{}",System.currentTimeMillis() - l);
            return categoryEntityVos;
        }

        List<CategoryEntityVo> categorysFromDBWithRedisLock = getCategorysFromDBWithRedissonLock();
//        List<CategoryEntityVo> categorysFromDBWithRedisLock = getCategorysFromDBWithRedisLock();
        String toJSONString = JSON.toJSONString(categorysFromDBWithRedisLock);
        int randounTime = 1440 + new Random().nextInt(5);
        redisTemplate.opsForValue().set("catalogJSON", toJSONString, randounTime, TimeUnit.MINUTES);
        log.info("业务执行完毕，消耗时间：{}",System.currentTimeMillis() - l);
        return categorysFromDBWithRedisLock;
    }

    /**
     * Redisson分布式锁
     * 缓存里的数据如何和数据库的数据保持一致？？
     * 缓存数据一致性
     * 1)、双写模式
     * 2)、失效模式
     * @return
     */
    public List<CategoryEntityVo> getCategorysFromDBWithRedissonLock() {
        //1、占分布式锁。去redis占坑
        //（锁的粒度，越细越快:具体缓存的是某个数据，11号商品） product-11-lock
        //RLock catalogJsonLock = redissonClient.getLock(RedisConstant.redis_category_json);
        //创建读锁
        RReadWriteLock readWriteLock = redissonClient.getReadWriteLock(RedisConstant.redis_category_json);

        RLock rLock = readWriteLock.readLock();
        List<CategoryEntityVo> categoryEntityVos = new ArrayList<>();
        try {
            rLock.lock();
            //加锁成功执行业务
            categoryEntityVos = getCategorysFromDB();
        }catch (Exception e){
            e.printStackTrace();
        }finally{
            rLock.unlock();
        }
        return categoryEntityVos;

    }
    /**
     * Redis分布式锁
     * @return
     */
    public List<CategoryEntityVo> getCategorysFromDBWithRedisLock() {
        String categoryJson = redisTemplate.opsForValue().get(RedisConstant.redis_category_json);
        List<CategoryEntityVo> categoryEntityVos = new ArrayList<>();
        if (StringUtils.isNotBlank(categoryJson)) {
            categoryEntityVos = JSON.parseArray(categoryJson, CategoryEntityVo.class);
            return categoryEntityVos;
        }
        //      1、占分布式锁。去Redis占坑
        String uuid = UUID.randomUUID().toString();
        Boolean look = redisTemplate.opsForValue().setIfAbsent(RedisConstant.redis_lock_key, uuid, 30, TimeUnit.SECONDS);
        if (look) {
            try {
                //            加锁成功，执行业务，释放锁
                categoryEntityVos = getCategorysFromDB();
                //            获取值对比+对比成功删除锁 = 原子操作
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                /**
                 * 不管业务执行成功与否，都给它执行释放锁
                 */
                Long unlock = redisTemplate.execute(new DefaultRedisScript<>(RedisConstant.redis_un_look_script, Long.class), Arrays.asList(RedisConstant.redis_lock_key, uuid));
                log.info("释放锁：" + unlock);
            }
        } else {
            log.info("获取分布式锁失败，进行重试............");
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
            }
            //            加锁失败则重试，类似于synchronized
            log.info("获取本地锁自旋");
            return getCatalogJsonFromDBWithLocalLock();
        }

        return categoryEntityVos;

    }

    //TODO 产生堆外内存溢出的错误：OutoDirectMemoryError
    public List<CategoryEntityVo> getCategorysFromDB() {
        log.info("开始查询数据库");
        long l = System.currentTimeMillis();
        List<CategoryEntity> categoryEntities = this.baseMapper.selectList(
                new QueryWrapper<CategoryEntity>().eq("parent_cid", 0));
        //将数据库的多次查询变为一次
        List<CategoryEntity> selectList = this.baseMapper.selectList(null);
        List<CategoryEntityVo> categoryEntityVoList = categoryEntities.stream().map(v -> {
            List<CategoryEntityVo> children = getChildren(v.getCatId(), selectList);

            CategoryEntityVo categoryEntityVo = new CategoryEntityVo();
            categoryEntityVo.setChildren(children);
            BeanUtils.copyProperties(v, categoryEntityVo);
            List<Long> count = children.stream().map(item -> {
                long size = 0;
                if (item.getChildren() != null && item.getChildren().size() > 0) {
                    int sum = item.getChildren().stream().map(item3 -> item3.getName().length()).mapToInt(Integer::intValue).sum();
                    size = sum;
                }
                return size;
            }).collect(Collectors.toList());
            long sum = count.stream().mapToLong(Long::longValue).sum();
            categoryEntityVo.setCategoryNameLength(sum);
            return categoryEntityVo;
        }).collect(Collectors.toList());
        log.info("查询数据库消耗时间：{}",System.currentTimeMillis() - l);
        return categoryEntityVoList;
    }

    private List<CategoryEntityVo> getChildren(Long pId, List<CategoryEntity> categoryEntityList) {
        List<CategoryEntityVo> categoryEntities = categoryEntityList.stream().filter(item -> item.getParentCid().equals(pId)).map(item -> {
            CategoryEntityVo entityVo = new CategoryEntityVo();
            BeanUtils.copyProperties(item, entityVo);
            return entityVo;
        }).collect(Collectors.toList());
        if (categoryEntities == null || categoryEntities.size() < 1) {
            return categoryEntities;
        }
        categoryEntities.stream().map(item -> {
            List<CategoryEntityVo> children = getChildren(item.getCatId(), categoryEntityList);
            item.setChildren(children);
            return item;
        }).collect(Collectors.toList());
        return categoryEntities;
    }


    /**
     * 本地锁
     *
     * @return
     */
    private List<CategoryEntityVo> getCatalogJsonFromDBWithLocalLock() {
        synchronized (this) {
            String catalogJSON = redisTemplate.opsForValue().get("catalogJSON");
            if (!StringUtils.isEmpty(catalogJSON)) {
                //缓存不为空直接返回
                List<CategoryEntityVo> result = JSON.parseArray(catalogJSON, CategoryEntityVo.class);
                return result;
            }
            return getCategorysFromDB();
        }

    }

    private List<Long> findParentPath(Long catelogId, List<Long> paths) {

        //1、收集当前节点id
        paths.add(catelogId);

        //根据当前分类id查询信息
        CategoryEntity byId = this.getById(catelogId);
        //如果当前不是父分类
        if (byId.getParentCid() != 0) {
            findParentPath(byId.getParentCid(), paths);
        }

        return paths;
    }

}