/** Copyright 2020 bejson.com */
package com.atguigu.gulimall.product.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * Auto-generated: 2020-05-31 11:3:26
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */

@Data
public class Attr implements Serializable {

  private Long attrId;
  private String attrName;
  private String attrValue;
}
