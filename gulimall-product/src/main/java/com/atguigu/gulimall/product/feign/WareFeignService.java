package com.atguigu.gulimall.product.feign;

import com.atguigu.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @author huangwei
 * @program: gulimall-master-my
 * @description: 远程仓库服务
 * @packagename: com.atguigu.gulimall.product.feign
 * @email 1142488172@qq.com
 * @date: 2022-04-28 15:01
 **/
@FeignClient("gulimall-ware")
public interface WareFeignService {

    @PostMapping(value = "/ware/waresku/hasStock")
    R getSkuHasStock(@RequestBody List<Long> skuIds);
}
