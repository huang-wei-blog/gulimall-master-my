package com.atguigu.gulimall.product.vo;

import lombok.Data;
import lombok.ToString;

/**
 * @author huangwei
 * @program: gulimall-master-my
 * @description:
 * @packagename: com.atguigu.common.to
 * @email 1142488172@qq.com
 * @date: 2022-04-08 09:22
 **/

@Data
@ToString
public class SpuBaseAttrVo {

    private String attrName;

    private String attrValue;

}
