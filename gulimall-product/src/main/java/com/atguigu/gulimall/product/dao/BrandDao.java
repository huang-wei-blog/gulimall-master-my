package com.atguigu.gulimall.product.dao;

import com.atguigu.gulimall.product.entity.BrandEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 品牌
 * 
 * @author huangwei
 * @email 1142488172@qq.com
 * @date 2022-03-08 17:37:22
 */
@Mapper
public interface BrandDao extends BaseMapper<BrandEntity> {
	
}
