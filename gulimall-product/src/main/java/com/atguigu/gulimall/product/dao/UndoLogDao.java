package com.atguigu.gulimall.product.dao;

import com.atguigu.gulimall.product.entity.UndoLogEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author huangwei
 * @email 1142488172@qq.com
 * @date 2022-03-08 17:37:22
 */
@Mapper
public interface UndoLogDao extends BaseMapper<UndoLogEntity> {
	
}
