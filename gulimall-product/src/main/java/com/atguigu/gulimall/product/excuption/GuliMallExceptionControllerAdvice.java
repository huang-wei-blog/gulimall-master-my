package com.atguigu.gulimall.product.excuption;

import com.atguigu.common.exception.BizCodeEnum;
import com.atguigu.common.utils.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.AbstractBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * 集中处理异常
 */
@Slf4j
//@ResponseBody
//@ControllerAdvice(basePackages = "com.atguigu.gulimall.product.controller")
//@Order(value = 1) 增加在spring bean中的加载优先级
@RestControllerAdvice(basePackages = "com.atguigu.gulimall.product.controller")
public class GuliMallExceptionControllerAdvice  {

    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public R handleValidataException(MethodArgumentNotValidException e){
        log.error("数据校验出现问题{}，异常类型：{}",e.getMessage(),e.getClass());
        BindingResult bindingResult = e.getBindingResult();
        Map<String,String> errorMap = new HashMap<>();
        bindingResult.getFieldErrors().forEach(fieldError -> {
            errorMap.put(fieldError.getField(),fieldError.getDefaultMessage());
        });
        return R.error(BizCodeEnum.VALIDATA_EXCEPTION.getCode(),BizCodeEnum.VALIDATA_EXCEPTION.getMsg()).put("data",errorMap);
    }

    @ExceptionHandler(value = Throwable.class)
    public R handleException(Throwable throwable){
        return R.error(BizCodeEnum.UNIKOWN_EXCEPTION.getCode(),BizCodeEnum.UNIKOWN_EXCEPTION.getMsg());
    }

}
