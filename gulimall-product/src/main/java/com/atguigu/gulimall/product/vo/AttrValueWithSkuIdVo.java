package com.atguigu.gulimall.product.vo;

import lombok.Data;

/**
 * @author huangwei
 * @program: gulimall-master-my
 * @description:
 * @packagename: com.atguigu.common.to
 * @email 1142488172@qq.com
 * @date: 2022-04-08 09:22
 **/

@Data
public class AttrValueWithSkuIdVo {

    private String attrValue;

    private String skuIds;

}
