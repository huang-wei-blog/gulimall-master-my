package com.atguigu.common.constant;

/**
 * @Author huangwei
 * @Date 2023/1/5
 * TODO Redis相关的常量
 */
public class RedisConstant {

    public static final String redis_lock_key = "lock";

    public static final String redis_category_json = "catalogJSON";

    public static final String redis_un_look_script = "if redis.call('get', KEYS[1]) == ARGV[1] then return redis.call('del', KEYS[1]) else return 0 end";
}
