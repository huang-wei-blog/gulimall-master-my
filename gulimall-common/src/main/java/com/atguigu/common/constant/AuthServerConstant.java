package com.atguigu.common.constant;

/*
 * @author huangwei
 * @emaill 1142488172@qq.com
 * @date 2023/4/1 12:25
 */

public class AuthServerConstant {
    public static final String SMS_CODE_CACHE_PREFIX = "sms:code:";

    public static final String LOGIN_USER = "loginUser";
}
