package com.atguigu.common.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author huangwei
 * @program: gulimall-master-my
 * @description: JsonBean互转工具
 * @packagename: com.atguigu.common.utils
 * @email 1142488172@qq.com
 * @date: 2022-04-30 16:19
 **/
public class JsonUtils<T> {


    public static <T> T copyProperties(Object source,Class<T> cla){
        if (null == source ||  cla == null){
            return null;
        }else {
            String json = JSON.toJSONString(source);
            T t = JSON.toJavaObject(JSONObject.parseObject(json), cla);
            return t;
        }
    }

    public static <T> List<T> convertToList(Object data,Class<T> cla){
        if (null == data){
            return null;
        }else if (null == cla){
            return null;
        }else {
            List<T> list = new ArrayList<>();
            if (data instanceof List){
                Iterator iterator = ((List) data).iterator();
                while (iterator.hasNext()){
                    String json = JSON.toJSONString(iterator.next());
                    T t = JSON.toJavaObject(JSONObject.parseObject(json),cla);
                    list.add(t);
                }
            }
            return list;
        }
    }


    public JSONObject mapToJson(Map<String, Object> map) {
        String data = JSON.toJSONString(map);
        return JSON.parseObject(data);
    }

    /**
     * map中取key对应的value
     *
     * @param map
     * @param key
     * @return
     */
    public String mapToString(Map<String, Object> map, String key) {
        JSONObject jsonObject = mapToJson(map);
        return jsonObject.getString(key);
    }

    /**
     * map转类对象
     *
     * @param map
     * @param clazz
     * @param <T>
     * @return
     */
    public <T> T mapToEntity(Map<String, Object> map, Class<T> clazz) {
        T t = null;
        JSONObject jsonObject = mapToJson(map);
        if (jsonObject != null) {
            t = jsonObject.toJavaObject(clazz);
        }
        return t;
    }

    public Map<String, Object> entityToMap(T t) {
        String s = JSON.toJSONString(t);
        Map<String, Object> parse = (Map<String, Object>) JSON.parse(s);
        return parse;
    }

    public Map<String, Object> entityToMapConstantDate(T t, String dateFormat) {
        String s = JSON.toJSONStringWithDateFormat(t, dateFormat);
        Map<String, Object> parse = (Map<String, Object>) JSON.parse(s);
        return parse;
    }

    /**
     * map中取类对象
     *
     * @param map
     * @param clazz
     * @param key
     * @param <T>
     * @return
     */
    public <T> T mapToObject(Map<String, Object> map, Class<T> clazz, String key) {
        T t = null;
        JSONObject jsonObject = mapToJson(map);
        JSONObject object = jsonObject.getJSONObject(key);
        if (object != null) {
            t = object.toJavaObject(clazz);
        }
        return t;
    }

    /**
     * map中取list
     *
     * @param map
     * @param clazz
     * @param key
     * @return
     */
    public List<T> mapToList(Map<String, Object> map, Class<T> clazz, String key) {
        List<T> t = null;
        JSONObject jsonObject = mapToJson(map);
        JSONArray array = jsonObject.getJSONArray(key);
        t = array.toJavaList(clazz);
        return t;
    }

    /**
     * 将 List<JavaBean>对象转化为List<Map>
     *
     * @return Object对象
     * @author loulan
     */
    public List<Map<String, Object>> convertListBean2ListMap(List<T> beanList) {
        List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();
        for (int i = 0, n = beanList.size(); i < n; i++) {
            T bean = beanList.get(i);
            Map<String, Object> map = entityToMap(bean);
            mapList.add(map);
        }
        return mapList;
    }

    /**
     * resultMap为一个包含日期类型数据的bean
     */
    public List<Map<String, Object>> convertListBeanConstantDateListMap(List<T> beanList, String dateFormat) {
        List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();
        for (int i = 0, n = beanList.size(); i < n; i++) {
            T bean = beanList.get(i);
            Map<String, Object> map = entityToMapConstantDate(bean, dateFormat);
            mapList.add(map);
        }
        return mapList;
    }

    /**
     * webScketJSon处理
     */
    private ObjectMapper mapper = new ObjectMapper();

    public String bean2Json(Object obj) {
        try {
            return mapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return "";
        }
    }


}
