package com.atguigu.common.to;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author huangwei
 * @program: gulimall-master-my
 * @description:
 * @packagename: com.atguigu.common.to
 * @email 1142488172@qq.com
 * @date: 2022-04-08 09:43
 **/
@Data
public class MemberPrice {
    private Long id;
    private String name;
    private BigDecimal price;
}
