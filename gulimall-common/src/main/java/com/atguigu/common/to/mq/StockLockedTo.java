package com.atguigu.common.to.mq;

import lombok.Data;

/*
 * @author huangwei
 * @emaill 1142488172@qq.com
 * @date 2023/4/3 23:37
 * 发送到mq消息队列的to
 */
@Data
public class StockLockedTo {
    /** 库存工作单的id **/
    private Long id;

    /** 工作单详情的所有信息 **/
    private StockDetailTo detailTo;
}
