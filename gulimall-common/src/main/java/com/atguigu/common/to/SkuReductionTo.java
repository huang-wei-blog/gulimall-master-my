package com.atguigu.common.to;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author huangwei
 * @program: gulimall-master-my
 * @description: 满减TO
 * @packagename: com.atguigu.common.to
 * @email 1142488172@qq.com
 * @date: 2022-04-08 09:41
 **/
@Data
public class SkuReductionTo {
    private Long skuId;
    private int fullCount;
    private BigDecimal discount;
    private int countStatus;
    private BigDecimal fullPrice;
    private BigDecimal reducePrice;
    private int priceStatus;
    private List<MemberPrice> memberPrice;
}
