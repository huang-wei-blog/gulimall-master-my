package com.atguigu.common.to;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author huangwei
 * @program: gulimall-master-my
 * @description: TO类
 * @packagename: com.atguigu.common.to
 * @email 1142488172@qq.com
 * @date: 2022-04-08 09:22
 **/
@Data
public class SpuBoundTo {
    private Long spuId;

    private BigDecimal buyBounds;

    private BigDecimal growBounds;

}
