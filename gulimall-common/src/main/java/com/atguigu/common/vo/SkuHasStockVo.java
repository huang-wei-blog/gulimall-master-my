package com.atguigu.common.vo;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * @author huangwei
 * @program: gulimall-master-my
 * @description: 商品检索ES模型
 * @packagename: com.atguigu.common.vo
 * @email 1142488172@qq.com
 * @date: 2022-04-28 15:06
 **/

@Data
public class SkuHasStockVo implements Serializable {

    private Long skuId;

    private Boolean hasStock;

}
