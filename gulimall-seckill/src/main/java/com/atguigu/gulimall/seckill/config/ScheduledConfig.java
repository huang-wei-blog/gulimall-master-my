package com.atguigu.gulimall.seckill.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @Description:
 * @author huangwei
 * @emaill 1142488172@qq.com
 * @date 2023/4/20 22:35
 **/

@EnableAsync
@EnableScheduling
@Configuration
public class ScheduledConfig {

}
