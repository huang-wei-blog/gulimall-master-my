import request from '@/utils/request'

export function getMemberDataList(params){
    return request({
        url: '/member/member/list',
        method: 'get',
        params: params
    })
}

export function getMemberInfo(id){
    return request({
        url: '/member/member/info/'+id,
        method: 'get',
    })
}

export function saveMember(data){
    return request({
        url: '/member/member/save',
        method: 'post',
        data: data
    })
}

export function updateMember(data){
    return request({
        url: '/member/member/update',
        method: 'post',
        data: data
    })
}

export function deleteMember(data){
    return request({
        url: '/member/member/delete',
        method: 'post',
        data: data
    })
}