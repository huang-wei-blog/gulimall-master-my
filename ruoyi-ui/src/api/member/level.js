import request from '@/utils/request'

export function getLevelDataList(params){
    return request({
        url: '/member/memberlevel/list',
        method: 'get',
        params: params
    })
}

export function getLevelInfo(id){
    return request({
        url: '/member/memberlevel/info/'+id,
        method: 'get',
    })
}

export function saveLevel(data){
    return request({
        url: '/member/memberlevel/save',
        method: 'post',
        data: data
    })
}

export function updateLevel(data){
    return request({
        url: '/member/memberlevel/update',
        method: 'post',
        data: data
    })
}

export function deleteLevel(data){
    return request({
        url: '/member/memberlevel/delete',
        method: 'post',
        data: data
    })
}