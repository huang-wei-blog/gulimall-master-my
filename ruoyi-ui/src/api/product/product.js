import request from '@/utils/request'

export function listCategoryTree(query) {
  return request({
    url: '/product/category/list/tree',
    method: 'get',
    params: query
  })
}

export function batchDelete(data) {
  return request({
    url: '/product/category/delete',
    method: 'post',
    data:data
  })
}

export function batchSave(data) {
  return request({
    url: '/product/category/update/sort',
    method: 'post',
    data:data
  })
}

export function edit(catId) {
  return request({
    url: '/product/category/info/'+catId,
    method: 'get',
  })
}

export function editCategory(data) {
  return request({
    url: '/product/category/update',
    method: 'post',
    data:data
  })
}

export function addCategory(data) {
  return request({
    url: '/product/category/save',
    method: 'post',
    data:data
  })
}

export function remove(data) {
  return request({
    url: '/product/category/delete',
    method: 'post',
    data:data
  })
}