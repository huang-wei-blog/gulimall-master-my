import request from '@/utils/request'

export function attrGroupInfo(attrGroupId,params) {
    return request({
        url: '/product/attrgroup/info/'+attrGroupId,
        method: 'get',
        params:params
    })
}


export function saveAttrGroup(data) {
    return request({
        url: '/product/attrgroup/save',
        method: 'post',
        data: data
    })
}

export function updateAttrGroup(data) {
    return request({
        url: '/product/attrgroup/update',
        method: 'post',
        data: data
    })
}

export function getDataAttrGroupList(params,cid) {
    return request({
        url: '/product/attrgroup/list/'+cid,
        method: 'get',
        params:params
    })
}

export function getAttrGroupWithAttrs(catalogId){
    return request({
        url:'/product/attrgroup/'+catalogId+'/withattr',
        method:'get'
    });
}

export function deleteAttrGroup(data) {
    return request({
        url: '/product/attrgroup/delete',
        method: 'post',
        data:data
    })
}