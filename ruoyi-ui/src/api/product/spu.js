import request from '@/utils/request'

export function updateUpStatus(id) {
    return request({
        url: '/product/spuinfo/' + id + '/up',
        method: 'post',
    })
}

export function spuInfoList(params) {
    return request({
        url: '/product/spuinfo/list',
        method: 'post',
        params: params
    })
}

export function saveSpu(data) {
    return request({
        url: '/product/spuinfo/save',
        method: 'post',
        data: data
    })
}