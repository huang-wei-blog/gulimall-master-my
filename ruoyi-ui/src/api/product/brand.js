import request from '@/utils/request'

export function addCatelogSelect(data) {
    return request({
        url: '/product/categorybrandrelation/save',
        method: 'post',
        data: data
    })
}

export function deleteCateRelationHandle(data) {
    return request({
        url: '/product/categorybrandrelation/delete',
        method: 'post',
        data: data
    })
}


export function getCateRelation(query) {
    return request({
        url: '/product/categorybrandrelation/catelog/list',
        method: 'get',
        params: query
    })
}

export function getCatBrands(query) {
    return request({
        url: '/product/categorybrandrelation/brands/list',
        method: 'get',
        params: query
    })
}

export function getDataList(query) {
    return request({
        url: '/product/brand/list',
        method: 'get',
        params: query
    })
}

export function getBrandInfo(query) {
    return request({
        url: '/product/brand/info/'+query,
        method: 'get',
        params:{}
    })
}

export function saveBrand(data) {
    return request({
        url: '/product/brand/save',
        method: 'post',
        data: data
    })
}

export function updateBrand(data) {
    return request({
        url: '/product/brand/update',
        method: 'post',
        data: data
    })
}

export function updateBrandStatus(data) {
    return request({
        url: '/product/brand/update/status',
        method: 'post',
        data: data
    })
}

export function deleteHandle(data) {
    return request({
        url: '/product/brand/delete',
        method: 'post',
        data: data
    })
}