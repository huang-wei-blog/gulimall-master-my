import request from '@/utils/request'

export function baseAttrlistforspu(spuId) {
    return request({
        url: '/product/attr/base/listforspu/'+spuId,
        method: 'get'
    })
}


export function getAttrByTypeCatIdList(type,catId,params) {
    return request({
        url: '/product/attr/'+type+'/list/'+catId,
        method: 'get',
        params:params
    })
}

export function getAttrInfo(params,attrId){
    return request({
        url:'/product/attr/info/'+attrId,
        method:'get',
        params:params
    })
}

export function baseAttrList(attrType,catelogId,params){
    return request({
        url:'/product/attr/'+attrType+'/list/'+catelogId,
        method:'get',
        params:params
    });
}


export function saveAttr(data) {
    return request({
        url: '/product/attr/save',
        method: 'post',
        data:data
    })
}

export function updateAttr(data) {
    return request({
        url: '/product/attr/update',
        method: 'post',
        data:data
    })
}

export function updateSpuAttr(data,spuId){
    return request({
        url:'/product/attr/update/'+spuId,
        method:'post',
        data:data
    })
}

export function deleteAttrByAttrId(data) {
    return request({
        url: '/product/attr/delete',
        method: 'post',
        data:data
    })
}