import request from '@/utils/request'

export function skuInfoList(params) {
    return request({
        url: '/product/skuinfo/list',
        method: 'post',
        params: params
    })
}
