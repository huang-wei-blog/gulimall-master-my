import request from '@/utils/request'

export function batchDeleteRelation(data) {
    return request({
        url: '/product/attrgroup/attr/relation/delete',
        method: 'post',
        data:data
    })
}

export function saveRealtion(data) {
    return request({
        url: '/product/attrgroup/attr/relation',
        method: 'post',
        data:data
    })
}

export function relationAttrList(attrGroupId) {
    return request({
        url: '/product/attrgroup/' + attrGroupId + '/attr/relation',
        method: 'get',
    })
}

export function relationList(params,attrGroupId) {
    return request({
        url: '/product/attrgroup/' + attrGroupId + '/noattr/relation',
        method: 'get',
        params:params
    })
}