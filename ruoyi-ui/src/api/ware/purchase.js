import request from '@/utils/request'
export function listPurchase(params){
    return request({
        url:'/ware/purchase/list',
        method: "get",
        params:params
    })
}

export function deletePurchase(data){
    return request({
        url:'/ware/purchase/delete',
        method: "post",
        data:data
    })
}

export function getPurchaseInfo(id) {
    return request({
        url: '/ware/purchase/info/'+id,
        method: 'get',
    })
}

export function savePurchase(data) {
    return request({
        url: '/ware/purchase/save',
        method: 'post',
        data:data
    })
}

export function updatePurchase(data){
    return request({
        url:'/ware/purchase/update',
        method: "post",
        data:data
    })
}

export function mergeItem(data){
    return request({
        url:'/ware/purchase/merge',
        method: "post",
        data:data
    })
}

export function getUnreceivedPurchaseList(params){
    return request({
        url:'/ware/purchase/unreceive/list',
        method: "get",
        params:params
    })
}
