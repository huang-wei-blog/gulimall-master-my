import request from '@/utils/request'

export function getWareList(params) {
    return request({
        url: '/ware/wareinfo/list',
        method: 'get',
        params:params
    })
}

export function getWareInfo(id) {
    return request({
        url: '/ware/wareinfo/info/'+id,
        method: 'get',
    })
}

export function saveWare(data) {
    return request({
        url: '/ware/wareinfo/save',
        method: 'post',
        data:data
    })
}

export function updateWare(data) {
    return request({
        url: '/ware/wareinfo/update',
        method: 'post',
        data:data
    })
}

export function deleteWare(data) {
    return request({
        url: '/ware/wareinfo/delete',
        method: 'post',
        data:data
    })
}