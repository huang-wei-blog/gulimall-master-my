import request from '@/utils/request'


export function getWareSkuList(params) {
    return request({
        url: '/ware/waresku/list',
        method: 'get',
        params:params
    })
}

export function getWareSkuInfo(id) {
    return request({
        url: '/ware/waresku/info/'+id,
        method: 'get',
    })
}

export function saveWareSku(data) {
    return request({
        url: '/ware/waresku/save',
        method: 'post',
        data:data
    })
}

export function updateWareSku(data) {
    return request({
        url: '/ware/waresku/update',
        method: 'post',
        data:data
    })
}

export function deleteWareSku(data) {
    return request({
        url: '/ware/waresku/delete',
        method: 'post',
        data:data
    })
}