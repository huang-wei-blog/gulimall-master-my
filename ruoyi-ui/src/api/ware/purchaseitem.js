import request from '@/utils/request'

export function getPurchasedetailList(params){
    return request({
        url:'/ware/purchasedetail/list',
        method:'get',
        params:params
    })
}

export function getInfoPurchasedetail(id){
    return request({
        url:'/ware/purchasedetail/info/'+id,
        method:'get'
    })
}

export function savePurchasedetail(data){
    return request({
        url:'/ware/purchasedetail/save',
        method:'post',
        data:data
    })
}

export function updatePurchasedetail(data){
    return request({
        url:'/ware/purchasedetail/update',
        method:'post',
        data:data
    })
}

export function deletePurchasedetail(data){
    return request({
        url:'/ware/purchasedetail/delete',
        method:'post',
        data:data
    })
}