import request from '@/utils/request'

export function getAllPeers(query) {
    return request({
        url: '/fastdfs/home/getAllPeers',
        method: 'get',
        params:query
    })
}

export function switchPeers(query) {
    return request({
        url: '/fastdfs/home/switchPeers',
        method: 'get',
        params:query
    })
}

export function repairStat(query) {
    return request({
        url: '/fastdfs/home/repair_stat',
        method: 'get',
        params:query
    })
}

export function removeEmptyDir(data) {
    return request({
        url: '/fastdfs/home/remove_empty_dir',
        method: 'post',
        data:data
    })
}

export function backup(data) {
    return request({
        url: '/fastdfs/home/backup',
        method: 'post',
        data:data
    })
}

export function repair(data) {
    return request({
        url: '/fastdfs/home/repair',
        method: 'post',
        data:data
    })
}

export function getStatus(query) {
    return request({
        url: '/fastdfs/home/getStatus',
        method: 'get',
        params:query
    })
}