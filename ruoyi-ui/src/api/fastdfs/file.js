import request from '@/utils/request'

export function getParentFile(query) {
    return request({
        url: '/fastdfs/file/getParentFile',
        method: 'get',
        params:query
    })
}

export function getDirFile(query) {
    return request({
        url: '/fastdfs/file/getDirFile',
        method: 'get',
        params:query
    })
}

export function getDetails(query) {
    return request({
        url: '/fastdfs/file/details',
        method: 'get',
        params:query
    })
}


export function deleteFile(data) {
    return request({
        url: '/fastdfs/file/deleteFile',
        method: 'post',
        params:data
    });
}