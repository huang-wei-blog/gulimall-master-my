package com.atguigu.gulimall.thirdparty.controller;

import com.atguigu.gulimall.thirdparty.to.PayTo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author huangwei
 * @Date 2023/4/4
 * TODO
 */
@RestController
@RequestMapping("/pay")
@Slf4j
public class PayController {
    @GetMapping(value = "/pay",consumes = "application/json")
    public String pay(@RequestBody PayTo to){
        log.info("调用远程支付");
        return "success";
    }
}
