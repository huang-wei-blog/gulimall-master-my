package com.ruoyi.fastdfs.controller;

import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import cn.hutool.system.OsInfo;
import cn.hutool.system.SystemUtil;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.Peers;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.fastdfs.common.Constant;
import com.ruoyi.fastdfs.common.DateUtil;
import com.ruoyi.fastdfs.service.IndexService;
import com.ruoyi.fastdfs.service.PeersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Perfree
 * @description 首页相关
 * @date 2021/3/22 16:14
 */
@RequestMapping("/fastdfs")
@Controller
public class IndexControllerFastDfs extends FastDfsBaseController {
    @Value("${ruoyi.version}")
    private String version;

    @Value("${ruoyi.copyrightYear}")
    private String versionDate;

    @Autowired
    private IndexService indexService;
    @Autowired
    private PeersService peersService;

    /**
     * @return com.perfree.common.ResponseBean
     * @description 获取状态信息
     * @author Perfree
     */
    @RequestMapping("/home/getStatus")
    @ResponseBody
    public AjaxResult getStatus() {
        Map<String,Object> data = new HashMap<>();
        try {
            OsInfo osInfo = SystemUtil.getOsInfo();
            data.put("osName", osInfo.getName());
            data.put("osArch", osInfo.getArch());
            data.put("version", version);
            data.put("versionDate", versionDate);
            SysUser user = SecurityUtils.getLoginUser().getUser();
            String tagetUrl = user.getPeers().getServerAddress()+"/"+user.getPeers().getGroupName();
            String json = HttpUtil.get(tagetUrl + Constant.API_STATUS);
            JSONObject parseObj = JSONUtil.parseObj(json);
            if (parseObj.get("status").equals(Constant.API_STATUS_SUCCESS)) {
                Map<String, Object> result = indexService.getStatus(parseObj.get("data"));
                data.putAll(result);
            } else {
                return AjaxResult.error("调取go-fastdfs接口失败");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return AjaxResult.success(data);
    }

    /**
     * @return com.perfree.common.ResponseBean
     * @description 修正统计信息(30天)
     * @author Perfree
     */
    @RequestMapping("/home/repair_stat")
    @ResponseBody
    public AjaxResult repair_stat() {
        int count = 0;
        for (int i = 0; i > -30; i--) {
            String dateStr = DateUtil.getFormatDate(DateUtil.StrToDate(DateUtil.dayAddOrCut(DateUtil.getFormatDate("yyyy-MM-dd"), i), "yyyy-MM-dd"), "yyyyMMdd");
            Map<String, Object> map = new HashMap<>(16);
            map.put("date", dateStr);
            String result = HttpUtil.post(getPeersUrl() + Constant.API_REPAIR_STAT, map);
            JSONObject parseObj = JSONUtil.parseObj(result);
            if (parseObj.get("status").equals("ok")) {
                count++;
            }
        }
        return AjaxResult.success("成功修正" + count + "天数据", null);
    }

    /**
     * @return com.perfree.common.ResponseBean
     * @description 删除空目录
     * @author Perfree
     */
    @RequestMapping("/home/remove_empty_dir")
    @ResponseBody
    public AjaxResult remove_empty_dir() {
        String result = HttpUtil.post(getPeersUrl() + Constant.API_REMOVE_EMPTY_DIR, new HashMap<>());
        JSONObject parseObj = JSONUtil.parseObj(result);
        if (parseObj.get("status").equals(Constant.API_STATUS_SUCCESS)) {
            return AjaxResult.success("操作成功,正在后台操作,请勿重复使用此功能", null);
        } else {
            return AjaxResult.error("操作失败,请稍后再试");
        }
    }

    /**
     * @return AjaxResult
     * @description 备份元数据, 30天
     * @author Perfree
     */
    @RequestMapping("/home/backup")
    @ResponseBody
    public AjaxResult backup() {
        int count = 0;
        for (int i = 0; i > -30; i--) {
            String subDateStr = DateUtil.dayAddOrCut(DateUtil.getFormatDate("yyyy-MM-dd"), i);
            String dateStr = DateUtil.getFormatDate(DateUtil.StrToDate(subDateStr, "yyyy-MM-dd"), "yyyyMMdd");
            Map<String, Object> map = new HashMap<>(16);
            map.put("date", dateStr);
            String result = HttpUtil.post(getPeersUrl() + Constant.API_BACKUP, map);
            JSONObject parseObj = JSONUtil.parseObj(result);
            if (parseObj.get("status").equals(Constant.API_STATUS_SUCCESS)) {
                count++;
            }
        }
        return AjaxResult.success("成功备份" + count + "天数据", null);
    }

   /**
    * @description 同步失败修复
    * @return com.perfree.common.ResponseBean
    * @author Perfree
    */
    @RequestMapping("/home/repair")
    @ResponseBody
    public AjaxResult repair() {
        String result = HttpUtil.post(getPeersUrl() + Constant.API_REPAIR + "?force=1", new HashMap<>());
        JSONObject parseObj = JSONUtil.parseObj(result);
        if (parseObj.get("status").equals(Constant.API_STATUS_SUCCESS)) {
            return error().success("操作成功,正在后台操作,请勿重复使用此功能", null);
        } else {
            return AjaxResult.error("操作失败,请稍后再试");
        }
    }

   /**
    * @description 获取所有集群
    * @return com.perfree.common.ResponseBean
    * @author Perfree
    */
    @GetMapping("/home/getAllPeers")
    @ResponseBody
    public AjaxResult getAllPeers(){
        List<Peers> peers = peersService.list();
        return AjaxResult.success(peers);
    }

    /**
     * @description 切换集群
     * @param id id
     * @param session session
     * @return com.perfree.common.ResponseBean
     * @author Perfree
     */
    @RequestMapping("/home/switchPeers")
    @ResponseBody
    public AjaxResult switchPeers(long id, HttpSession session){
        if(id == getPeers().getPeersId()){
            return AjaxResult.error("当前正在使用此集群");
        }
//        User user = new User();
//        user.setPeersId(id);
//        user.setId(getUser().getId());
//        user.setUpdateTime(new Date());
//        if(userService.updateById(user)){
//            Peers peers = peersService.getById(id);
//            session.setAttribute("peers",peers);
//            return ResponseBean.success();
//        }
        return AjaxResult.error("切换失败");
    }

}
