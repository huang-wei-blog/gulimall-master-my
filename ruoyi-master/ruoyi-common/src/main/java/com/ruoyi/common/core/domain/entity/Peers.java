package com.ruoyi.common.core.domain.entity;


import java.io.Serializable;
import java.util.Date;

/**
 * @description peers集群表实体类
 * @author Perfree
 * @date 2021/3/22 14:35
 */
public class Peers implements Serializable {

    private static final long serialVersionUID = -554407295275500627L;
    private Long peersId;
    private String name;
    private String groupName;
    private String serverAddress;
    private String showAddress;
    private Date createTime;
    private Date updateTime;

    public Long getPeersId() {
        return peersId;
    }

    public void setPeersId(Long peersId) {
        this.peersId = peersId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getServerAddress() {
        return serverAddress;
    }

    public void setServerAddress(String serverAddress) {
        this.serverAddress = serverAddress;
    }

    public String getShowAddress() {
        return showAddress;
    }

    public void setShowAddress(String showAddress) {
        this.showAddress = showAddress;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
