package com.atguigu.gulimall.search.controller;

import com.atguigu.common.utils.R;
import com.atguigu.gulimall.search.service.MallSearchService;
import com.atguigu.gulimall.search.vo.SearchParam;
import com.atguigu.gulimall.search.vo.SearchResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;

//@RestController
//@RequestMapping("search")
@Controller
public class SearchController {
    @Autowired
    private MallSearchService mallSearchService;


    /**
     * 自动将页面提交过来的所有请求参数封装成我们指定的对象
     * @param param
     * @return
     */
//    @GetMapping(value = "/list")
//    public R listPage(SearchParam param, HttpServletRequest request) {
//
//        param.set_queryString(request.getQueryString());
//
//        //1、根据传递来的页面的查询参数，去es中检索商品
//        SearchResult result = mallSearchService.search(param);
//        Map<String,Object> data = new HashMap<>();
//        data.put("result",result);
//        return R.ok(data);
//    }

//    @GetMapping(value = {"/","search.html"})
//    public String indexPage(SearchParam param, Model model, HttpServletRequest request) {
//
//        param.set_queryString(request.getQueryString());
//
//        //1、根据传递来的页面的查询参数，去es中检索商品
//        SearchResult result = mallSearchService.search(param);
//
//        model.addAttribute("result",result);
//
//        return "list";
//    }

    /**
     * 自动将页面提交过来的所有请求参数封装成我们指定的对象
     * @param param
     * @return
     */
    @GetMapping(value = {"/search.html","/"})
    public String getSearchPage(SearchParam searchParam, Model model, HttpServletRequest request) {
        searchParam.set_queryString(request.getQueryString());
        SearchResult result=mallSearchService.thymeleafSearch(searchParam);
        model.addAttribute("result", result);
        return "search";
    }
}
