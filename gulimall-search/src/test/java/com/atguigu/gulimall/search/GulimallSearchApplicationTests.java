package com.atguigu.gulimall.search;

import com.alibaba.fastjson.JSON;
import com.atguigu.common.to.MemberPrice;
import com.atguigu.common.to.SpuBoundTo;
import com.atguigu.gulimall.search.config.GulimallElasticSearchConfig;
import lombok.Data;
import lombok.ToString;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.Avg;
import org.elasticsearch.search.aggregations.metrics.AvgAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.Sum;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Map;

@SpringBootTest
@RunWith(SpringRunner.class)
class GulimallSearchApplicationTests {

    @Autowired
    private RestHighLevelClient client;

    @Test
    public void indexData() throws IOException {
        IndexRequest indexRequest = new IndexRequest ("users");

        MemberPrice memberPrice = new MemberPrice();
        memberPrice.setName("张三");
        memberPrice.setPrice(BigDecimal.valueOf(123));
        memberPrice.setId(123L);
        String jsonString = JSON.toJSONString(memberPrice);
        //设置要保存的内容
//        indexRequest.source(jsonString, XContentType.JSON);
//        //执行创建索引和保存数据
//        IndexResponse index = client.index(indexRequest, GulimallElasticSearchConfig.COMMON_OPTIONS);

//        System.out.println(index);
    }

    @Test
    public void searchData() throws IOException {
        GetRequest getRequest = new GetRequest(
                "users",
                "qtLZYoABd7QKRj9XSa1c");

        GetResponse getResponse = client.get(getRequest, RequestOptions.DEFAULT);
        System.out.println(getResponse);
        String index = getResponse.getIndex();
        System.out.println(index);
        String id = getResponse.getId();
        System.out.println(id);
        if (getResponse.isExists()) {
            long version = getResponse.getVersion();
            System.out.println(version);
            String sourceAsString = getResponse.getSourceAsString();
            System.out.println(sourceAsString);
            Map<String, Object> sourceAsMap = getResponse.getSourceAsMap();
            System.out.println(sourceAsMap);
            byte[] sourceAsBytes = getResponse.getSourceAsBytes();
        } else {

        }
    }

    /**
     * 复杂检索:在bank中搜索address中包含mill的所有人的年龄分布以及平均年龄，平均薪资
     * @throws IOException
     */
    @Test
    public void searchDataAnalysis() throws IOException {
//        1、创建检索请求
        SearchRequest searchRequest = new SearchRequest();
//            1.1）指定索引
        searchRequest.indices("newblank");
//            1.2）构建索引条件
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        sourceBuilder.query(QueryBuilders.matchQuery("address","mill"));
//                1.2.1）按照年龄分布进行聚合
        TermsAggregationBuilder ageAgg = AggregationBuilders.terms("ageAgg").field("age").size(100);
        sourceBuilder.aggregation(ageAgg);
//                1.2.2）计算平均年龄
        AvgAggregationBuilder ageAvg = AggregationBuilders.avg("ageAvg").field("age");
        sourceBuilder.aggregation(ageAvg);
//                1.2.3）计算平均薪资
        AvgAggregationBuilder blanceAvg = AggregationBuilders.avg("balanceAvg").field("balance");
        sourceBuilder.aggregation(blanceAvg);

        searchRequest.source(sourceBuilder);
        System.out.println("检索条件："+sourceBuilder);
//        2、执行检索
        SearchResponse searchResponse = client.search(searchRequest,RequestOptions.DEFAULT);
        System.out.println("检索结果："+searchResponse);
//        3、将检索结果封装为Bean
        SearchHits hits = searchResponse.getHits();
        SearchHit[] searchHits = hits.getHits();
        for (SearchHit seachHit :searchHits) {
            String sourceAsString = seachHit.getSourceAsString();
            Account account = JSON.parseObject(sourceAsString, Account.class);
            System.out.println(account.toString());
        }
//        4、获取聚合信息
        Aggregations aggregations = searchResponse.getAggregations();
        Terms ageAgg1 = aggregations.get("ageAgg");
        for (Terms.Bucket bucket:ageAgg1.getBuckets()) {
            String keyAsString = bucket.getKeyAsString();
            System.out.println("年龄："+keyAsString+"====>"+bucket.getDocCount());
        }
        Avg ageAvg1 = aggregations.get("ageAvg");
        System.out.println("平均年龄："+ageAvg1.getValue());

        Avg blanceAvg1 =aggregations.get("balanceAvg");
        System.out.println("平均薪资："+blanceAvg1.getValue());


    }

    /**
     * 查出所有年龄分布，并且这些年龄段中M的平均薪资和F的平均薪资以及这个年龄段的总体平均薪资
     */

    @Test
    @Order(value = 1)
    public void searchBlankGroupAgeSexBlance() throws IOException {
//        1、创建检索请求
        SearchRequest searchRequest = new SearchRequest();
        searchRequest.indices("blank");
//            1.1）组装查询条件
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(QueryBuilders.matchAllQuery()).size(0);

        TermsAggregationBuilder aggregation = AggregationBuilders.terms("ageGroup")
                .field("age").size(5);
        TermsAggregationBuilder genderBuilder = AggregationBuilders.terms("genderGroup").field("gender.keyword");
        genderBuilder.subAggregation(AggregationBuilders.avg("genderBalance").field("balance"));
        aggregation.subAggregation(genderBuilder);
        aggregation.subAggregation(AggregationBuilders.sum("balanceSum").field("balance"));

        searchSourceBuilder.aggregation(aggregation);

        searchRequest.source(searchSourceBuilder);
        System.out.println("检索条件："+searchSourceBuilder);
//        2、执行检索
        SearchResponse searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
//        3、将检索结果封装为Bean
        SearchHits hits = searchResponse.getHits();
        SearchHit[] searchHits = hits.getHits();
        System.out.println("样本总数=========>："+hits.getTotalHits().value);
        for (SearchHit hit : searchHits) {
            String sourceAsString = hit.getSourceAsString();
            Account account = JSON.parseObject(sourceAsString, Account.class);
            System.out.println("样本值域======>："+account.toString());
        }
//        4、获取聚合信息
        Aggregations aggregations = searchResponse.getAggregations();
        Terms ageGroup = aggregations.get("ageGroup");
        System.out.println("没有统计到文档的个数========>"+ageGroup.getSumOfOtherDocCounts());
        for (Terms.Bucket bucket:ageGroup.getBuckets()) {

            System.out.println(bucket.getKey()+"岁年龄分组总数："+bucket.getDocCount());

            Sum genderSum = bucket.getAggregations().get("balanceSum");
            System.out.println(bucket.getKey()+"岁年龄分组总薪资："+genderSum.getValue());

            Terms genderGroup = bucket.getAggregations().get("genderGroup");
            for (Terms.Bucket gender:genderGroup.getBuckets()) {
                System.out.println(bucket.getKey()+"岁年龄分组性别为："+gender.getKey()+"，分组总数有"+gender.getDocCount());
                Avg avg = gender.getAggregations().get("genderBalance");
                System.out.println(bucket.getKey()+"岁年龄分组性别为："+gender.getKey()+"，分组下的平均薪资"+avg.getValue());

            }

        }

    }


    @Test
    void contextLoads() {
        System.out.println(client);

    }

    @ToString
    @Data
    static class Account {
        private int account_number;
        private int balance;
        private String firstname;
        private String lastname;
        private int age;
        private String gender;
        private String address;
        private String employer;
        private String email;
        private String city;
        private String state;
    }

}
