package com.atguigu.gulimall.member.feign;

import com.atguigu.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * 它会先去寻找上面的服务，然后调用下面的方法
 */
@FeignClient("gulimall-coupon")
public interface CoupFeginService {
    @RequestMapping("coupon/coupon/member/list")
    public R mermbercoupons();
}
