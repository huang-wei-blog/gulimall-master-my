package com.atguigu.gulimall.member.vo;

import lombok.Data;

/**
 * @author huangwei
 * @program: gulimall-master-my
 * @description:社交用户信息
 * @packagename: com.atguigu.common.to
 * @email 1142488172@qq.com
 * @date: 2022-04-08 09:22
 **/

@Data
public class SocialUser {

    private String access_token;

    private String remind_in;

    private long expires_in;

    private String uid;

    private String isRealName;

}
