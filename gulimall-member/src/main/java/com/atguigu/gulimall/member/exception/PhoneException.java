package com.atguigu.gulimall.member.exception;

/**
 * @author huangwei
 * @program: gulimall-master-my
 * @description:
 * @packagename: com.atguigu.common.to
 * @email 1142488172@qq.com
 * @date: 2022-04-08 09:22
 **/
public class PhoneException extends RuntimeException {

    public PhoneException() {
        super("存在相同的手机号");
    }
}
