package com.atguigu.gulimall.member.vo;

import lombok.Data;

/**
 * @author huangwei
 * @program: gulimall-master-my
 * @description:
 * @packagename: com.atguigu.common.to
 * @email 1142488172@qq.com
 * @date: 2022-04-08 09:22
 **/

@Data
public class MemberUserRegisterVo {

    private String userName;

    private String password;

    private String phone;

}
