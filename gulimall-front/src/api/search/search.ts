import request from '@/utils/request';
import { ElasticSearchParams } from '../common/ParamsEntity';


export function getGoodsList(params:ElasticSearchParams) {
    return request({
        url: '/search/list',
        method: 'get',
        params:params
    })
}