// interface ElasticSearchParams{
//   keyword:string,
//   brandId:[],
//   catalog3Id:[],
//   sort:number,
//   hasStock:number,
//   skuPrice:string,
//   attrs:[],
//   pageNum:number,
//   _queryString:string
// }
export class ElasticSearchParams{
  public keyword:string | undefined;
  public brandId:[] | undefined;
  public catalog3Id:[]| undefined;
  public sort:string| undefined;
  public hasStock:number| undefined;
  public skuPrice:string| undefined;
  public attrs:[]| undefined;
  public  pageNum:number| undefined;
  public _queryString:string| undefined
}