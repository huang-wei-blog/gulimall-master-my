/** 统一返回结构体 */

export interface CrudResponseEntity<T = any> {
  data: T;
  code?: number;
  isSuccess?: boolean;
  msg: string;
  timestamp: number;
}

export interface ListResponseEntity<T = any> {
  data: T[];
  code?: number;
  isSuccess?: boolean;
  msg: string;
  timestamp: number;
}

export interface ErrorResponseEntity {
  type: string;
  title: string;
  status: number;
  detail: string;
  path: string;
  message: string;
}