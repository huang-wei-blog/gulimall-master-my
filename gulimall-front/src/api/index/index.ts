import request from '@/utils/request';


export function getCategoryIndexPage() {
    return request({
        url: '/product/',
        method: 'get',
    })
}