package com.atguigu.gulimall.cart.to;

import lombok.Data;

/*
 * @author huangwei
 * @emaill 1142488172@qq.com
 * @date 2023/4/3 21:40
 */
@Data
public class UserInfoTo {
    private Long userId;

    private String userKey;

    /**
     * 是否临时用户
     */
    private Boolean tempUser = false;
}
