package com.atguigu.gulimall.order.vo;

import lombok.Data;

/*
 * @author huangwei
 * @emaill 1142488172@qq.com
 * @date 2023/4/3 23:31
 * 库存vo
 */
@Data
public class SkuStockVo {


    private Long skuId;

    private Boolean hasStock;

}
