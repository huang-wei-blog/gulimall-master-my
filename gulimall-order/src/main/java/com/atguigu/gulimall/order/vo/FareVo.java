package com.atguigu.gulimall.order.vo;

import lombok.Data;

import java.math.BigDecimal;

/*
 * @author huangwei
 * @emaill 1142488172@qq.com
 * @date 2023/4/3 23:27
 */

@Data
public class FareVo {

    private MemberAddressVo address;

    private BigDecimal fare;

}
