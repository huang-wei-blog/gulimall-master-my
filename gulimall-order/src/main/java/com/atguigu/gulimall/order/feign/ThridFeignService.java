package com.atguigu.gulimall.order.feign;

import com.alipay.api.AlipayApiException;
import com.atguigu.gulimall.order.vo.PayVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;

/*
 * @author huangwei
 * @emaill 1142488172@qq.com
 * @date 2023/4/1 23:19
 */

@FeignClient("gulimall-third-party")
public interface ThridFeignService {

    @GetMapping(value = "/pay",consumes = "application/json")
    String pay(@RequestBody PayVo vo) throws AlipayApiException;

}
