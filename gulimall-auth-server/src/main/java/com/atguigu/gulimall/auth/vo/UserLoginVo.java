package com.atguigu.gulimall.auth.vo;

import lombok.Data;

import java.io.Serializable;

/*
 * @author huangwei
 * @emaill 1142488172@qq.com
 * @date 2023/4/1 12:15
 */
@Data
public class UserLoginVo implements Serializable {
    private String loginacct;

    private String password;
}
