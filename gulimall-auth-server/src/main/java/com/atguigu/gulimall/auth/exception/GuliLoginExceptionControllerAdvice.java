package com.atguigu.gulimall.auth.exception;

import com.atguigu.common.exception.BizCodeEnum;
import com.atguigu.common.utils.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/*
 * @author huangwei
 * @emaill 1142488172@qq.com
 * @date 2023/4/1 22:53
 */
@Slf4j
@RestControllerAdvice(basePackages = "com.atguigu.gulimall.auth.controller")
public class GuliLoginExceptionControllerAdvice {

    @ExceptionHandler(value = RuntimeException.class)
    public R handleException(RuntimeException throwable){
        return R.error(BizCodeEnum.PHONE_IS_NOT_NULL.getCode(),BizCodeEnum.PHONE_IS_NOT_NULL.getMsg());
    }
}
