package com.atguigu.gulimall.auth.vo;

import lombok.Data;
import java.io.Serializable;
/*
 * @author huangwei
 * @emaill 1142488172@qq.com
 * @date 2023/4/1 12:14
 */
@Data
public class SocialUser implements Serializable {

    private String access_token;

    private String remind_in;

    private long expires_in;

    private String uid;

    private String isRealName;
}
