package com.atguigu.gulimall.ware;

import com.atguigu.gulimall.ware.entity.WareInfoEntity;
import com.atguigu.gulimall.ware.service.WareInfoService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class GulimallWareApplicationTests {

    @Autowired
    WareInfoService wareInfoService;

    @Test
    void contextLoads() {
        List<WareInfoEntity> list = wareInfoService.list(new QueryWrapper<WareInfoEntity>().eq("id", 1L));
        list.forEach(node->{
            System.out.println(node);
        });
    }

}
