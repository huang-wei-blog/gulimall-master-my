package com.atguigu.gulimall.ware.vo;

import lombok.Data;

import java.util.List;

/*
 * @author huangwei
 * @emaill 1142488172@qq.com
 * @date 2023/4/5 0:03
 */
@Data
public class SkuLockVo {
    private Long skuId;
    private Integer num;
    private List<Long> wareIds;

}
