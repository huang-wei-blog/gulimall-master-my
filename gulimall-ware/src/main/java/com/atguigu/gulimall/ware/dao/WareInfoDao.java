package com.atguigu.gulimall.ware.dao;

import com.atguigu.gulimall.ware.entity.WareInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 仓库信息
 * 
 * @author huangwei
 * @email 1142488172@qq.com
 * @date 2022-03-11 16:49:13
 */
@Mapper
public interface WareInfoDao extends BaseMapper<WareInfoEntity> {
	
}
