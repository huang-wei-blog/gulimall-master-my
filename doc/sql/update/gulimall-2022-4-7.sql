USE gulimall_ums;
ALTER TABLE ums_member MODIFY expires_in bigint(11) COMMENT '社交账号Token过期时间';