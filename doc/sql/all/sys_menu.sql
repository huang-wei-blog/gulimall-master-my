/*
 Navicat Premium Data Transfer

 Source Server         : 谷粒
 Source Server Type    : MySQL
 Source Server Version : 50736
 Source Host           : 192.168.1.111:3306
 Source Schema         : gulimall_admin

 Target Server Type    : MySQL
 Target Server Version : 50736
 File Encoding         : 65001

 Date: 18/04/2022 16:33:34
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '菜单名称',
  `parent_id` bigint(20) NULL DEFAULT 0 COMMENT '父菜单ID',
  `order_num` int(4) NULL DEFAULT 0 COMMENT '显示顺序',
  `path` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '路由地址',
  `component` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '组件路径',
  `query` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '路由参数',
  `is_frame` int(1) NULL DEFAULT 1 COMMENT '是否为外链（0是 1否）',
  `is_cache` int(1) NULL DEFAULT 0 COMMENT '是否缓存（0缓存 1不缓存）',
  `menu_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
  `visible` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '菜单状态（0正常 1停用）',
  `perms` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '#' COMMENT '菜单图标',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2047 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '菜单权限表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, '系统管理', 0, 99, 'system', NULL, '', 1, 0, 'M', '0', '0', '', 'system', 'admin', '2022-03-19 01:48:18', 'admin', '2022-03-24 12:47:53', '系统管理目录');
INSERT INTO `sys_menu` VALUES (2, '系统监控', 0, 999, 'monitor', NULL, '', 1, 0, 'M', '0', '0', '', 'monitor', 'admin', '2022-03-19 01:48:18', 'admin', '2022-03-24 12:48:01', '系统监控目录');
INSERT INTO `sys_menu` VALUES (3, '系统工具', 0, 999, 'tool', NULL, '', 1, 0, 'M', '0', '0', '', 'tool', 'admin', '2022-03-19 01:48:18', 'admin', '2022-03-24 12:48:09', '系统工具目录');
INSERT INTO `sys_menu` VALUES (100, '用户管理', 1, 1, 'user', 'system/user/index', '', 1, 0, 'C', '0', '0', 'system:user:list', 'user', 'admin', '2022-03-19 01:48:18', '', NULL, '用户管理菜单');
INSERT INTO `sys_menu` VALUES (101, '角色管理', 1, 2, 'role', 'system/role/index', '', 1, 0, 'C', '0', '0', 'system:role:list', 'peoples', 'admin', '2022-03-19 01:48:18', '', NULL, '角色管理菜单');
INSERT INTO `sys_menu` VALUES (102, '菜单管理', 1, 3, 'menu', 'system/menu/index', '', 1, 0, 'C', '0', '0', 'system:menu:list', 'tree-table', 'admin', '2022-03-19 01:48:18', '', NULL, '菜单管理菜单');
INSERT INTO `sys_menu` VALUES (103, '部门管理', 1, 4, 'dept', 'system/dept/index', '', 1, 0, 'C', '0', '0', 'system:dept:list', 'tree', 'admin', '2022-03-19 01:48:18', '', NULL, '部门管理菜单');
INSERT INTO `sys_menu` VALUES (104, '岗位管理', 1, 5, 'post', 'system/post/index', '', 1, 0, 'C', '0', '0', 'system:post:list', 'post', 'admin', '2022-03-19 01:48:18', '', NULL, '岗位管理菜单');
INSERT INTO `sys_menu` VALUES (105, '字典管理', 1, 6, 'dict', 'system/dict/index', '', 1, 0, 'C', '0', '0', 'system:dict:list', 'dict', 'admin', '2022-03-19 01:48:18', '', NULL, '字典管理菜单');
INSERT INTO `sys_menu` VALUES (106, '参数设置', 1, 7, 'config', 'system/config/index', '', 1, 0, 'C', '0', '0', 'system:config:list', 'edit', 'admin', '2022-03-19 01:48:18', '', NULL, '参数设置菜单');
INSERT INTO `sys_menu` VALUES (107, '通知公告', 1, 8, 'notice', 'system/notice/index', '', 1, 0, 'C', '0', '0', 'system:notice:list', 'message', 'admin', '2022-03-19 01:48:18', '', NULL, '通知公告菜单');
INSERT INTO `sys_menu` VALUES (108, '日志管理', 1, 9, 'log', '', '', 1, 0, 'M', '0', '0', '', 'log', 'admin', '2022-03-19 01:48:18', '', NULL, '日志管理菜单');
INSERT INTO `sys_menu` VALUES (109, '在线用户', 2, 1, 'online', 'monitor/online/index', '', 1, 0, 'C', '0', '0', 'monitor:online:list', 'online', 'admin', '2022-03-19 01:48:18', '', NULL, '在线用户菜单');
INSERT INTO `sys_menu` VALUES (110, '定时任务', 2, 2, 'job', 'monitor/job/index', '', 1, 0, 'C', '0', '0', 'monitor:job:list', 'job', 'admin', '2022-03-19 01:48:18', '', NULL, '定时任务菜单');
INSERT INTO `sys_menu` VALUES (111, '数据监控', 2, 3, 'druid', 'monitor/druid/index', '', 1, 0, 'C', '0', '0', 'monitor:druid:list', 'druid', 'admin', '2022-03-19 01:48:18', '', NULL, '数据监控菜单');
INSERT INTO `sys_menu` VALUES (112, '服务监控', 2, 4, 'server', 'monitor/server/index', '', 1, 0, 'C', '0', '0', 'monitor:server:list', 'server', 'admin', '2022-03-19 01:48:18', '', NULL, '服务监控菜单');
INSERT INTO `sys_menu` VALUES (113, '缓存监控', 2, 5, 'cache', 'monitor/cache/index', '', 1, 0, 'C', '0', '0', 'monitor:cache:list', 'redis', 'admin', '2022-03-19 01:48:18', '', NULL, '缓存监控菜单');
INSERT INTO `sys_menu` VALUES (114, '表单构建', 3, 1, 'build', 'tool/build/index', '', 1, 0, 'C', '0', '0', 'tool:build:list', 'build', 'admin', '2022-03-19 01:48:18', '', NULL, '表单构建菜单');
INSERT INTO `sys_menu` VALUES (115, '代码生成', 3, 2, 'gen', 'tool/gen/index', '', 1, 0, 'C', '0', '0', 'tool:gen:list', 'code', 'admin', '2022-03-19 01:48:18', '', NULL, '代码生成菜单');
INSERT INTO `sys_menu` VALUES (116, '系统接口', 3, 3, 'swagger', 'tool/swagger/index', '', 1, 0, 'C', '0', '0', 'tool:swagger:list', 'swagger', 'admin', '2022-03-19 01:48:18', '', NULL, '系统接口菜单');
INSERT INTO `sys_menu` VALUES (500, '操作日志', 108, 1, 'operlog', 'monitor/operlog/index', '', 1, 0, 'C', '0', '0', 'monitor:operlog:list', 'form', 'admin', '2022-03-19 01:48:18', '', NULL, '操作日志菜单');
INSERT INTO `sys_menu` VALUES (501, '登录日志', 108, 2, 'logininfor', 'monitor/logininfor/index', '', 1, 0, 'C', '0', '0', 'monitor:logininfor:list', 'logininfor', 'admin', '2022-03-19 01:48:18', '', NULL, '登录日志菜单');
INSERT INTO `sys_menu` VALUES (1001, '用户查询', 100, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:user:query', '#', 'admin', '2022-03-19 01:48:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1002, '用户新增', 100, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:user:add', '#', 'admin', '2022-03-19 01:48:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1003, '用户修改', 100, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:user:edit', '#', 'admin', '2022-03-19 01:48:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1004, '用户删除', 100, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:user:remove', '#', 'admin', '2022-03-19 01:48:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1005, '用户导出', 100, 5, '', '', '', 1, 0, 'F', '0', '0', 'system:user:export', '#', 'admin', '2022-03-19 01:48:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1006, '用户导入', 100, 6, '', '', '', 1, 0, 'F', '0', '0', 'system:user:import', '#', 'admin', '2022-03-19 01:48:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1007, '重置密码', 100, 7, '', '', '', 1, 0, 'F', '0', '0', 'system:user:resetPwd', '#', 'admin', '2022-03-19 01:48:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1008, '角色查询', 101, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:role:query', '#', 'admin', '2022-03-19 01:48:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1009, '角色新增', 101, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:role:add', '#', 'admin', '2022-03-19 01:48:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1010, '角色修改', 101, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:role:edit', '#', 'admin', '2022-03-19 01:48:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1011, '角色删除', 101, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:role:remove', '#', 'admin', '2022-03-19 01:48:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1012, '角色导出', 101, 5, '', '', '', 1, 0, 'F', '0', '0', 'system:role:export', '#', 'admin', '2022-03-19 01:48:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1013, '菜单查询', 102, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:query', '#', 'admin', '2022-03-19 01:48:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1014, '菜单新增', 102, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:add', '#', 'admin', '2022-03-19 01:48:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1015, '菜单修改', 102, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:edit', '#', 'admin', '2022-03-19 01:48:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1016, '菜单删除', 102, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:remove', '#', 'admin', '2022-03-19 01:48:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1017, '部门查询', 103, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:query', '#', 'admin', '2022-03-19 01:48:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1018, '部门新增', 103, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:add', '#', 'admin', '2022-03-19 01:48:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1019, '部门修改', 103, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:edit', '#', 'admin', '2022-03-19 01:48:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1020, '部门删除', 103, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:remove', '#', 'admin', '2022-03-19 01:48:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1021, '岗位查询', 104, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:post:query', '#', 'admin', '2022-03-19 01:48:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1022, '岗位新增', 104, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:post:add', '#', 'admin', '2022-03-19 01:48:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1023, '岗位修改', 104, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:post:edit', '#', 'admin', '2022-03-19 01:48:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1024, '岗位删除', 104, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:post:remove', '#', 'admin', '2022-03-19 01:48:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1025, '岗位导出', 104, 5, '', '', '', 1, 0, 'F', '0', '0', 'system:post:export', '#', 'admin', '2022-03-19 01:48:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1026, '字典查询', 105, 1, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:query', '#', 'admin', '2022-03-19 01:48:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1027, '字典新增', 105, 2, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:add', '#', 'admin', '2022-03-19 01:48:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1028, '字典修改', 105, 3, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:edit', '#', 'admin', '2022-03-19 01:48:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1029, '字典删除', 105, 4, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:remove', '#', 'admin', '2022-03-19 01:48:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1030, '字典导出', 105, 5, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:export', '#', 'admin', '2022-03-19 01:48:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1031, '参数查询', 106, 1, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:query', '#', 'admin', '2022-03-19 01:48:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1032, '参数新增', 106, 2, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:add', '#', 'admin', '2022-03-19 01:48:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1033, '参数修改', 106, 3, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:edit', '#', 'admin', '2022-03-19 01:48:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1034, '参数删除', 106, 4, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:remove', '#', 'admin', '2022-03-19 01:48:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1035, '参数导出', 106, 5, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:export', '#', 'admin', '2022-03-19 01:48:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1036, '公告查询', 107, 1, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:query', '#', 'admin', '2022-03-19 01:48:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1037, '公告新增', 107, 2, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:add', '#', 'admin', '2022-03-19 01:48:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1038, '公告修改', 107, 3, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:edit', '#', 'admin', '2022-03-19 01:48:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1039, '公告删除', 107, 4, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:remove', '#', 'admin', '2022-03-19 01:48:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1040, '操作查询', 500, 1, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:operlog:query', '#', 'admin', '2022-03-19 01:48:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1041, '操作删除', 500, 2, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:operlog:remove', '#', 'admin', '2022-03-19 01:48:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1042, '日志导出', 500, 4, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:operlog:export', '#', 'admin', '2022-03-19 01:48:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1043, '登录查询', 501, 1, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:query', '#', 'admin', '2022-03-19 01:48:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1044, '登录删除', 501, 2, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:remove', '#', 'admin', '2022-03-19 01:48:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1045, '日志导出', 501, 3, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:export', '#', 'admin', '2022-03-19 01:48:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1046, '在线查询', 109, 1, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:online:query', '#', 'admin', '2022-03-19 01:48:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1047, '批量强退', 109, 2, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:online:batchLogout', '#', 'admin', '2022-03-19 01:48:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1048, '单条强退', 109, 3, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:online:forceLogout', '#', 'admin', '2022-03-19 01:48:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1049, '任务查询', 110, 1, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:query', '#', 'admin', '2022-03-19 01:48:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1050, '任务新增', 110, 2, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:add', '#', 'admin', '2022-03-19 01:48:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1051, '任务修改', 110, 3, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:edit', '#', 'admin', '2022-03-19 01:48:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1052, '任务删除', 110, 4, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:remove', '#', 'admin', '2022-03-19 01:48:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1053, '状态修改', 110, 5, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:changeStatus', '#', 'admin', '2022-03-19 01:48:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1054, '任务导出', 110, 7, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:export', '#', 'admin', '2022-03-19 01:48:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1055, '生成查询', 115, 1, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:query', '#', 'admin', '2022-03-19 01:48:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1056, '生成修改', 115, 2, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:edit', '#', 'admin', '2022-03-19 01:48:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1057, '生成删除', 115, 3, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:remove', '#', 'admin', '2022-03-19 01:48:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1058, '导入代码', 115, 2, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:import', '#', 'admin', '2022-03-19 01:48:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1059, '预览代码', 115, 4, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:preview', '#', 'admin', '2022-03-19 01:48:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1060, '生成代码', 115, 5, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:code', '#', 'admin', '2022-03-19 01:48:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2000, '商品系统', 0, 1, 'product', NULL, NULL, 1, 0, 'M', '0', '0', '', 'shopping', 'admin', '2022-03-18 22:58:29', 'admin', '2022-03-18 23:02:57', '');
INSERT INTO `sys_menu` VALUES (2001, '分类维护', 2000, 1, 'category', 'product/category/category', NULL, 1, 0, 'C', '0', '0', '', 'tree-table', 'admin', '2022-03-18 23:02:21', 'admin', '2022-03-25 09:44:09', '');
INSERT INTO `sys_menu` VALUES (2002, '品牌管理', 2000, 2, 'brand', 'product/brand/brand', '', 1, 0, 'C', '0', '0', '', 'list', 'admin', '2022-03-25 09:10:59', 'admin', '2022-03-25 09:44:35', '');
INSERT INTO `sys_menu` VALUES (2003, '文件系统', 0, 98, 'fastdfs', NULL, NULL, 1, 0, 'M', '0', '0', NULL, 'client', 'admin', '2022-03-26 13:28:55', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2004, '控制台', 2003, 1, 'home', 'fastdfs/home/index', NULL, 1, 0, 'C', '0', '0', NULL, 'dashboard', 'admin', '2022-03-26 13:31:47', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2005, 'gofast文档', 2003, 99, 'https://sjqzhang.gitee.io/go-fastdfs/QA.html', NULL, NULL, 0, 0, 'C', '0', '0', '', 'education', 'admin', '2022-03-27 18:14:07', 'admin', '2022-03-31 10:17:12', '');
INSERT INTO `sys_menu` VALUES (2006, '文件管理', 2003, 3, 'file', NULL, NULL, 1, 0, 'M', '0', '0', '', 'upload', 'admin', '2022-03-27 19:23:48', 'admin', '2022-03-27 19:25:37', '');
INSERT INTO `sys_menu` VALUES (2007, '文件列表', 2006, 1, 'file', 'fastdfs/file/index', NULL, 1, 0, 'C', '0', '0', '', 'download', 'admin', '2022-03-27 19:24:38', 'admin', '2022-03-27 19:26:07', '');
INSERT INTO `sys_menu` VALUES (2008, '平台属性', 2000, 3, 'attrgroup', NULL, NULL, 1, 0, 'M', '0', '0', NULL, 'system', 'admin', '2022-04-06 02:27:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2009, '属性分组', 2008, 1, 'attrgroup', 'product/attrgroup/attrgroup', NULL, 1, 0, 'C', '0', '0', NULL, 'chart', 'admin', '2022-04-06 02:28:47', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2010, '规格参数', 2008, 2, 'baseattr', 'product/baseattr/baseattr', NULL, 1, 0, 'C', '0', '0', NULL, 'documentation', 'admin', '2022-04-06 02:31:38', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2011, '销售属性', 2008, 3, 'saleattr', 'product/saleattr/saleattr', NULL, 1, 0, 'C', '0', '0', '', 'table', 'admin', '2022-04-06 02:32:25', 'admin', '2022-04-06 13:20:39', '');
INSERT INTO `sys_menu` VALUES (2012, '商品维护', 2000, 4, 'spu', NULL, NULL, 1, 0, 'M', '0', '0', NULL, 'dict', 'admin', '2022-04-07 01:10:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2013, '用户系统', 0, 2, 'member', NULL, NULL, 1, 0, 'M', '0', '0', NULL, 'user', 'admin', '2022-04-07 01:22:13', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2014, '会员列表', 2013, 1, 'member', 'member/member/member', NULL, 1, 0, 'C', '0', '0', NULL, 'user', 'admin', '2022-04-07 01:25:54', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2015, '会员等级', 2013, 2, 'level', 'member/level/level', NULL, 1, 0, 'C', '0', '0', '', 'chart', 'admin', '2022-04-07 01:27:01', 'admin', '2022-04-07 01:41:30', '');
INSERT INTO `sys_menu` VALUES (2016, '积分变化', 2013, 3, 'integral', 'member/integral/integral', NULL, 1, 0, 'C', '0', '0', NULL, 'star', 'admin', '2022-04-07 01:42:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2017, '统计信息', 2013, 4, 'total', 'member/total/total', NULL, 1, 0, 'C', '0', '0', NULL, 'chart', 'admin', '2022-04-07 01:43:15', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2018, 'SPU管理', 2012, 1, 'spu', 'product/spu/spu', NULL, 1, 0, 'C', '0', '0', NULL, 'dict', 'admin', '2022-04-07 08:32:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2019, '商品管理', 2012, 3, 'manager', 'product/manager/manager', NULL, 1, 0, 'C', '0', '0', '', 'edit', 'admin', '2022-04-07 08:33:33', 'admin', '2022-04-07 08:36:07', '');
INSERT INTO `sys_menu` VALUES (2020, '发布商品', 2012, 2, 'wares', 'product/wares/spuadd', NULL, 1, 0, 'C', '0', '0', NULL, 'edit', 'admin', '2022-04-07 08:35:19', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2021, '库存系统', 0, 3, 'ware', NULL, NULL, 1, 0, 'M', '0', '0', '', 'excel', 'admin', '2022-04-09 03:31:33', 'admin', '2022-04-09 03:51:23', '');
INSERT INTO `sys_menu` VALUES (2022, '仓库维护', 2021, 1, 'wareinfo', 'ware/ware/wareinfo', NULL, 1, 0, 'C', '0', '0', '', 'excel', 'admin', '2022-04-09 03:32:08', 'admin', '2022-04-09 04:08:40', '');
INSERT INTO `sys_menu` VALUES (2023, '库存工作单', 2021, 2, 'task', 'ware/task/task', NULL, 1, 0, 'C', '0', '0', '', 'documentation', 'admin', '2022-04-09 03:32:33', 'admin', '2022-04-09 03:53:39', '');
INSERT INTO `sys_menu` VALUES (2024, '商品库存', 2021, 3, 'sku', 'ware/sku/sku', NULL, 1, 0, 'C', '0', '0', '', 'lock', 'admin', '2022-04-09 03:32:54', 'admin', '2022-04-09 03:54:49', '');
INSERT INTO `sys_menu` VALUES (2025, '订单系统', 0, 4, 'order', '', NULL, 1, 0, 'M', '0', '0', '', '#', 'admin', '2022-04-09 03:34:30', 'admin', '2022-04-09 03:35:38', '');
INSERT INTO `sys_menu` VALUES (2026, '订单查询', 2025, 1, 'order', 'order/order/order', NULL, 1, 0, 'C', '0', '0', NULL, '#', 'admin', '2022-04-09 03:36:03', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2027, '退货单处理', 2025, 2, 'return', 'order/return/return', NULL, 1, 0, 'C', '0', '0', '', '#', 'admin', '2022-04-09 03:36:30', 'admin', '2022-04-09 03:37:06', '');
INSERT INTO `sys_menu` VALUES (2028, '等级规则', 2025, 3, 'settings', 'order/settings/settings', NULL, 1, 0, 'C', '0', '0', '', '#', 'admin', '2022-04-09 03:36:54', 'admin', '2022-04-09 03:37:12', '');
INSERT INTO `sys_menu` VALUES (2029, '支付流水查询', 2025, 4, 'payment', 'order/payment/payment', NULL, 1, 0, 'C', '0', '0', NULL, '#', 'admin', '2022-04-09 03:37:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2030, '退款流水查询', 2025, 5, 'refund', 'order/refund/refund', NULL, 1, 0, 'C', '0', '0', NULL, '#', 'admin', '2022-04-09 03:38:11', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2031, '内容管理', 0, 5, 'sousuo', NULL, NULL, 1, 0, 'M', '0', '0', NULL, '#', 'admin', '2022-04-09 03:40:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2032, '首页推荐', 2031, 1, 'index', 'content/index', NULL, 1, 0, 'C', '0', '0', NULL, '#', 'admin', '2022-04-09 03:40:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2033, '分类热门', 2031, 2, 'category', 'content/category', NULL, 1, 0, 'C', '0', '0', NULL, '#', 'admin', '2022-04-09 03:41:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2034, '评论管理', 2031, 3, 'comments', 'content/comments', NULL, 1, 0, 'C', '0', '0', NULL, '#', 'admin', '2022-04-09 03:41:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2035, '优惠营销', 0, 6, 'coupon', NULL, NULL, 1, 0, 'M', '0', '0', NULL, '#', 'admin', '2022-04-09 03:43:12', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2036, '优惠券管理', 2035, 1, 'coupon', 'coupon/coupon/coupon', NULL, 1, 0, 'C', '0', '0', NULL, '#', 'admin', '2022-04-09 03:43:41', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2037, '发放记录', 2035, 2, 'history', 'coupon/history/history', NULL, 1, 0, 'C', '0', '0', NULL, '#', 'admin', '2022-04-09 03:45:07', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2038, '专题活动', 2035, 3, 'subject', 'coupon/subject/subject', NULL, 1, 0, 'C', '0', '0', NULL, '#', 'admin', '2022-04-09 03:45:31', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2039, '秒杀活动', 2035, 4, 'seckill', 'coupon/seckill/seckill', NULL, 1, 0, 'C', '0', '0', NULL, '#', 'admin', '2022-04-09 03:45:56', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2040, '积分维护', 2035, 5, 'bounds', 'coupon/bounds/bounds', NULL, 1, 0, 'C', '0', '0', NULL, '#', 'admin', '2022-04-09 03:46:20', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2041, '满减折扣', 2035, 6, 'full', 'coupon/full/full', NULL, 1, 0, 'C', '0', '0', NULL, '#', 'admin', '2022-04-09 03:46:40', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2042, '会员价格', 2035, 7, 'memberprice', 'coupon/memberprice/memberprice', NULL, 1, 0, 'C', '0', '0', NULL, '#', 'admin', '2022-04-09 03:47:07', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2043, '每日秒杀', 2035, 8, 'seckillsession', 'coupon/seckillsession/seckillsession', NULL, 1, 0, 'C', '0', '0', NULL, '#', 'admin', '2022-04-09 03:47:31', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2044, '采购单维护', 2021, 4, 'tubiao', '', NULL, 1, 0, 'M', '0', '0', '', 'chart', 'admin', '2022-04-09 03:48:19', 'admin', '2022-04-09 03:57:05', '');
INSERT INTO `sys_menu` VALUES (2045, '采购需求', 2044, 1, 'purchaseitem', 'ware/purchaseitem/purchaseitem', NULL, 1, 0, 'C', '0', '0', '', 'documentation', 'admin', '2022-04-09 03:48:44', 'admin', '2022-04-09 17:56:42', '');
INSERT INTO `sys_menu` VALUES (2046, '采购单', 2044, 2, 'purchase', 'ware/purchase/purchase', NULL, 1, 0, 'C', '0', '0', '', 'list', 'admin', '2022-04-09 03:49:05', 'admin', '2022-04-09 17:56:50', '');

SET FOREIGN_KEY_CHECKS = 1;
