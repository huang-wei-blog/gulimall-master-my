/*
 Navicat Premium Data Transfer

 Source Server         : hadoop3.0
 Source Server Type    : MySQL
 Source Server Version : 50736
 Source Host           : 192.168.159.102:3306
 Source Schema         : nacos_config

 Target Server Type    : MySQL
 Target Server Version : 50736
 File Encoding         : 65001

 Date: 20/04/2023 00:00:08
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for config_info
-- ----------------------------
DROP TABLE IF EXISTS `config_info`;
CREATE TABLE `config_info`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `data_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'data_id',
  `group_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `content` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'content',
  `md5` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'md5',
  `gmt_create` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `gmt_modified` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  `src_user` text CHARACTER SET utf8 COLLATE utf8_bin NULL COMMENT 'source user',
  `src_ip` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'source ip',
  `app_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `tenant_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '租户字段',
  `c_desc` varchar(256) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `c_use` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `effect` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `type` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `c_schema` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `encrypted_data_key` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '秘钥',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_configinfo_datagrouptenant`(`data_id`, `group_id`, `tenant_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 22 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = 'config_info' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of config_info
-- ----------------------------
INSERT INTO `config_info` VALUES (1, 'datasource.yml', 'dev', 'spring:\r\n  datasource:\r\n    driver-class-name: com.mysql.jdbc.Driver\r\n    username: root\r\n    password: 123456\r\n    url: jdbc:mysql://192.168.159.102:3306/gulimall_wms?useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=false&serverTimezone=GMT%2B8', '76e3e61f609d5692fdc09f35eabbb17a', '2023-04-05 14:11:21', '2023-04-05 14:11:21', NULL, '192.168.159.1', '', '8f0278b6-d6b7-412e-8c03-06dd8f71b72b', NULL, NULL, NULL, 'yaml', NULL, '');
INSERT INTO `config_info` VALUES (2, 'mybatis.yml', 'dev', 'mybatis-plus:\r\n  mapper-location: classpath:/mapper/**/*.xml\r\n  global-config:\r\n    db-config:\r\n      id-type: auto\r\n', 'c872fb01f56a08a482a41f73fa3f6094', '2023-04-05 14:11:21', '2023-04-05 14:11:21', NULL, '192.168.159.1', '', '8f0278b6-d6b7-412e-8c03-06dd8f71b72b', NULL, NULL, NULL, 'yaml', NULL, '');
INSERT INTO `config_info` VALUES (3, 'other.yml', 'dev', 'spring:\r\n  cloud:\r\n    nacos:\r\n      discovery:\r\n        server-addr: 127.0.0.1:8848\r\n  application:\r\n    name: gulimall-ware\r\n# 日志配置\r\nlogging:\r\n  level:\r\n    com.atguigu: debug\r\n    org.springframework: warn\r\n\r\nserver:\r\n  port: 11000', '874def5119d5c6da1adafdd03d1f9d27', '2023-04-05 14:11:21', '2023-04-05 14:11:21', NULL, '192.168.159.1', '', '8f0278b6-d6b7-412e-8c03-06dd8f71b72b', NULL, NULL, NULL, 'yaml', NULL, '');
INSERT INTO `config_info` VALUES (4, 'gulimall-gateway.yml', 'DEFAULT_GROUP', 'spring:\r\n  application:\r\n    name: gulimall-gateway', '994cf84dbc3a8974a55026081dc689ae', '2023-04-05 14:13:51', '2023-04-05 14:13:51', NULL, '192.168.159.1', '', 'add4416d-3003-47a5-ac25-ee349cb42915', NULL, NULL, NULL, 'yaml', NULL, '');
INSERT INTO `config_info` VALUES (5, 'datasource.yml', 'dev', 'spring:\r\n  datasource:\r\n    driver-class-name: com.mysql.jdbc.Driver\r\n    username: root\r\n    password: 123456\r\n    url: jdbc:mysql://192.168.159.102:3306/gulimall_pms?useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=false&serverTimezone=GMT%2B8\r\n  cloud:\r\n    nacos:\r\n      discovery:\r\n        server-addr: 127.0.0.1:8848\r\n  jackson:\r\n    date-format: yyy-MM-dd HH:mm:ss\r\n  application:\r\n    name: gulimall-product\r\n  #    一定要关缓存，否则会因为缓存原因导致热加载未更新\r\n  thymeleaf:\r\n    cache: false\r\n  redis:\r\n    host: 192.168.159.102\r\n    port: 6379', '019efe5c126a4bae9101eac3e5d5576e', '2023-04-05 14:14:14', '2023-04-05 14:14:14', NULL, '192.168.159.1', '', 'bb16d17e-fea5-4bd0-afcd-2aa199301315', NULL, NULL, NULL, 'yaml', NULL, '');
INSERT INTO `config_info` VALUES (6, 'mybatis.yml', 'dev', 'mybatis-plus:\r\n  mapper-location: classpath:/mapper/**/*.xml\r\n  global-config:\r\n    db-config:\r\n      id-type: auto\r\n', 'c872fb01f56a08a482a41f73fa3f6094', '2023-04-05 14:14:14', '2023-04-05 14:14:14', NULL, '192.168.159.1', '', 'bb16d17e-fea5-4bd0-afcd-2aa199301315', NULL, NULL, NULL, 'yaml', NULL, '');
INSERT INTO `config_info` VALUES (7, 'other.yml', 'dev', 'spring:\r\n  cloud:\r\n    nacos:\r\n      discovery:\r\n        server-addr: 127.0.0.1:8848\r\n  application:\r\n    name: gulimall-product\r\n# 日志配置\r\nlogging:\r\n  level:\r\n    com.atguigu: debug\r\n    org.springframework: warn\r\n\r\nserver:\r\n  port: 10010', '749510bd8e9356440e5b6788543de1f6', '2023-04-05 14:14:14', '2023-04-05 14:14:14', NULL, '192.168.159.1', '', 'bb16d17e-fea5-4bd0-afcd-2aa199301315', NULL, NULL, NULL, 'yaml', NULL, '');
INSERT INTO `config_info` VALUES (8, 'datasource.yml', 'dev', '# 数据源配置\r\nspring:\r\n  datasource:\r\n    type: com.alibaba.druid.pool.DruidDataSource\r\n    driverClassName: com.mysql.cj.jdbc.Driver\r\n    druid:\r\n      # 主库数据源\r\n      master:\r\n        url: jdbc:mysql://192.168.159.102:3306/gulimall_admin?useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=false&serverTimezone=GMT%2B8\r\n        username: root\r\n        password: 123456\r\n      # 从库数据源\r\n      slave:\r\n        # 从数据源开关/默认关闭\r\n        enabled: false\r\n        url:\r\n        username:\r\n        password:\r\n      # 初始连接数\r\n      initialSize: 5\r\n      # 最小连接池数量\r\n      minIdle: 10\r\n      # 最大连接池数量\r\n      maxActive: 20\r\n      # 配置获取连接等待超时的时间\r\n      maxWait: 60000\r\n      # 配置间隔多久才进行一次检测，检测需要关闭的空闲连接，单位是毫秒\r\n      timeBetweenEvictionRunsMillis: 60000\r\n      # 配置一个连接在池中最小生存的时间，单位是毫秒\r\n      minEvictableIdleTimeMillis: 300000\r\n      # 配置一个连接在池中最大生存的时间，单位是毫秒\r\n      maxEvictableIdleTimeMillis: 900000\r\n      # 配置检测连接是否有效\r\n      validationQuery: SELECT 1 FROM DUAL\r\n      testWhileIdle: true\r\n      testOnBorrow: false\r\n      testOnReturn: false\r\n      webStatFilter:\r\n        enabled: true\r\n      statViewServlet:\r\n        enabled: true\r\n        # 设置白名单，不填则允许所有访问\r\n        allow:\r\n        url-pattern: /druid/*\r\n        # 控制台管理用户名和密码\r\n        login-username: ruoyi\r\n        login-password: 123456\r\n      filter:\r\n        stat:\r\n          enabled: true\r\n          # 慢SQL记录\r\n          log-slow-sql: true\r\n          slow-sql-millis: 1000\r\n          merge-sql: true\r\n        wall:\r\n          config:\r\n            multi-statement-allow: true', 'b802bd44e7a3e05236a8791c321e0db3', '2023-04-05 14:14:31', '2023-04-05 14:14:31', NULL, '192.168.159.1', '', '36576576-18d5-4a83-81c9-b8c0f4fe385b', NULL, NULL, NULL, 'yaml', NULL, '');
INSERT INTO `config_info` VALUES (9, 'mybatis.yml', 'dev', '# MyBatis配置\r\nmybatis:\r\n    # 搜索指定包别名\r\n    typeAliasesPackage: com.ruoyi.**.domain\r\n    # 配置mapper的扫描，找到所有的mapper.xml映射文件\r\n    mapperLocations: classpath*:mapper/**/*Mapper.xml\r\n    # 加载全局的配置文件\r\n    configLocation: classpath:mybatis/mybatis-config.xml\r\n\r\n# PageHelper分页插件\r\npagehelper:\r\n  helperDialect: mysql\r\n  supportMethodsArguments: true\r\n  params: count=countSql', '0fc9ae5a0fdc6d825bfbdbf6754d8f00', '2023-04-05 14:14:31', '2023-04-05 14:14:31', NULL, '192.168.159.1', '', '36576576-18d5-4a83-81c9-b8c0f4fe385b', NULL, NULL, NULL, 'yaml', NULL, '');
INSERT INTO `config_info` VALUES (10, 'other.yml', 'dev', '# 项目相关配置\r\nruoyi:\r\n  # 名称\r\n  name: RuoYi\r\n  # 版本\r\n  version: 3.8.1\r\n  # 版权年份\r\n  copyrightYear: 2022\r\n  # 实例演示开关\r\n  demoEnabled: true\r\n  # 文件路径 示例（ Windows配置D:/ruoyi/uploadPath，Linux配置 /home/ruoyi/uploadPath）\r\n  profile: D:/ruoyi/uploadPath\r\n  # 获取ip地址开关\r\n  addressEnabled: false\r\n  # 验证码类型 math 数组计算 char 字符验证\r\n  captchaType: math\r\n  fastdfs: \r\n    path: /temp/\r\n\r\n# 开发环境配置\r\nserver:\r\n  # 服务器的HTTP端口，默认为8080\r\n  port: 8081\r\n  servlet:\r\n    # 应用的访问路径\r\n    context-path: /ruoyi\r\n  tomcat:\r\n    # tomcat的URI编码\r\n    uri-encoding: UTF-8\r\n    # 连接数满后的排队数，默认为100\r\n    accept-count: 1000\r\n    threads:\r\n      # tomcat最大线程数，默认为200\r\n      max: 800\r\n      # Tomcat启动初始化的线程数，默认值10\r\n      min-spare: 100\r\n\r\n# 日志配置\r\nlogging:\r\n  level:\r\n    com.ruoyi: debug\r\n    org.springframework: warn\r\n\r\n# Spring配置\r\nspring:\r\n  # 资源信息\r\n  messages:\r\n    # 国际化资源文件路径\r\n    basename: i18n/messages\r\n  # 文件上传\r\n  servlet:\r\n     multipart:\r\n       # 单个文件大小\r\n       max-file-size:  10MB\r\n       # 设置总上传的文件大小\r\n       max-request-size:  20MB\r\n  # 服务模块\r\n  devtools:\r\n    restart:\r\n      # 热部署开关\r\n      enabled: true\r\n  # redis 配置\r\n  redis:\r\n    # 地址\r\n    host: 192.168.159.102\r\n    # 端口，默认为6379\r\n    port: 6379\r\n    # 数据库索引\r\n    database: 0\r\n    # 密码\r\n    password:\r\n    # 连接超时时间\r\n    timeout: 10s\r\n    lettuce:\r\n      pool:\r\n        # 连接池中的最小空闲连接\r\n        min-idle: 0\r\n        # 连接池中的最大空闲连接\r\n        max-idle: 8\r\n        # 连接池的最大数据库连接数\r\n        max-active: 8\r\n        # #连接池最大阻塞等待时间（使用负值表示没有限制）\r\n        max-wait: -1ms\r\n        #注册中心\r\n  cloud:\r\n    nacos:\r\n      discovery:\r\n        server-addr: 127.0.0.1:8848\r\n  application:\r\n    name: gulimall-ruoyi\r\n  mvc:\r\n    pathmatch:\r\n      matching-strategy: ant_path_matcher\r\n\r\n# token配置\r\ntoken:\r\n    # 令牌自定义标识\r\n    header: Authorization\r\n    # 令牌密钥\r\n    secret: abcdefghijklmnopqrstuvwxyz\r\n    # 令牌有效期（默认30分钟）\r\n    expireTime: 30\r\n\r\n# Swagger配置\r\nswagger:\r\n  # 是否开启swagger\r\n  enabled: true\r\n  # 请求前缀\r\n  pathMapping: /dev-api\r\n\r\n# 防止XSS攻击\r\nxss:\r\n  # 过滤开关\r\n  enabled: true\r\n  # 排除链接（多个用逗号分隔）\r\n  excludes: /system/notice\r\n  # 匹配链接\r\n  urlPatterns: /system/*,/monitor/*,/tool/*\r\n', 'd026098f848fe75330b410f1107b3527', '2023-04-05 14:14:31', '2023-04-05 14:14:31', NULL, '192.168.159.1', '', '36576576-18d5-4a83-81c9-b8c0f4fe385b', NULL, NULL, NULL, 'yaml', NULL, '');
INSERT INTO `config_info` VALUES (11, 'datasource.yml', 'dev', 'spring:\r\n  datasource:\r\n    driver-class-name: com.mysql.jdbc.Driver\r\n    username: root\r\n    password: 123456\r\n    url: jdbc:mysql://192.168.159.102:3306/gulimall_ums?useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=false&serverTimezone=GMT%2B8', '51505ed02f20934d3d6cadee10f69002', '2023-04-05 14:14:57', '2023-04-05 14:14:57', NULL, '192.168.159.1', '', '832d69fc-3d7f-4d2b-803c-61d0f52ec6f3', NULL, NULL, NULL, 'yaml', NULL, '');
INSERT INTO `config_info` VALUES (12, 'mybatis.yml', 'dev', 'mybatis-plus:\r\n  mapper-location: classpath:/mapper/**/*.xml\r\n  global-config:\r\n    db-config:\r\n      id-type: auto\r\n', 'c872fb01f56a08a482a41f73fa3f6094', '2023-04-05 14:14:57', '2023-04-05 14:14:57', NULL, '192.168.159.1', '', '832d69fc-3d7f-4d2b-803c-61d0f52ec6f3', NULL, NULL, NULL, 'yaml', NULL, '');
INSERT INTO `config_info` VALUES (13, 'other.yml', 'dev', 'spring:\r\n  cloud:\r\n    nacos:\r\n      discovery:\r\n        server-addr: 127.0.0.1:8848\r\n  application:\r\n    name: gulimall-member\r\n# 日志配置\r\nlogging:\r\n  level:\r\n    com.atguigu: debug\r\n    org.springframework: warn\r\n\r\nserver:\r\n  port: 8000', '8ae41f88bbf8dcd4474d9b8c4a51ddb9', '2023-04-05 14:14:57', '2023-04-05 14:14:57', NULL, '192.168.159.1', '', '832d69fc-3d7f-4d2b-803c-61d0f52ec6f3', NULL, NULL, NULL, 'yaml', NULL, '');
INSERT INTO `config_info` VALUES (14, 'gulimall-coupon.properties', 'DEFAULT_GROUP', 'coupon.user.name = huangwei\r\ncoupon.user.age = 23', '20d2c4eef3c148e7c277d5069e90a9b0', '2023-04-05 14:15:11', '2023-04-05 14:15:11', NULL, '192.168.159.1', '', '09e41474-0479-403e-a8b5-529cd8f1f9af', NULL, NULL, NULL, 'properties', NULL, '');
INSERT INTO `config_info` VALUES (15, 'datasource.yml', 'dev', 'spring:\r\n  datasource:\r\n    driver-class-name: com.mysql.jdbc.Driver\r\n    username: root\r\n    password: 123456\r\n    url: jdbc:mysql://192.168.159.102:3306/gulimall_sms?useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=false&serverTimezone=GMT%2B8\r\n    ', 'bfd79d5b64729d80568701a0c39d1e27', '2023-04-05 14:15:11', '2023-04-05 14:15:11', NULL, '192.168.159.1', '', '09e41474-0479-403e-a8b5-529cd8f1f9af', NULL, NULL, NULL, 'yaml', NULL, '');
INSERT INTO `config_info` VALUES (16, 'mybatis.yml', 'dev', 'mybatis-plus:\r\n  mapper-location: classpath:/mapper/**/*.xml\r\n  global-config:\r\n    db-config:\r\n      id-type: auto\r\n', 'c872fb01f56a08a482a41f73fa3f6094', '2023-04-05 14:15:11', '2023-04-05 14:15:11', NULL, '192.168.159.1', '', '09e41474-0479-403e-a8b5-529cd8f1f9af', NULL, NULL, NULL, 'yaml', NULL, '');
INSERT INTO `config_info` VALUES (17, 'other.yml', 'dev', 'spring:\r\n  cloud:\r\n    nacos:\r\n      discovery:\r\n        server-addr: 127.0.0.1:8848\r\n  application:\r\n    name: gulimall-coupon\r\n    \r\nserver:\r\n  port: 7000\r\n#        下面这种方式不知道配置中心可不可以正常启动\r\n#      config:\r\n#        server-addr: 127.0.0.1:8848', '1a4b6a3ec2299afafe23c4522b7ea6d6', '2023-04-05 14:15:11', '2023-04-05 14:15:11', NULL, '192.168.159.1', '', '09e41474-0479-403e-a8b5-529cd8f1f9af', NULL, NULL, NULL, 'yaml', NULL, '');
INSERT INTO `config_info` VALUES (21, 'seataServer.properties', 'SEATA_GROUP', '# 存储模式\r\nstore.mode=db\r\n \r\nstore.db.datasource=druid\r\nstore.db.dbType=mysql\r\n# 需要根据mysql的版本调整driverClassName\r\n# mysql8及以上版本对应的driver：com.mysql.cj.jdbc.Driver\r\n# mysql8以下版本的driver：com.mysql.jdbc.Driver\r\nstore.db.driverClassName=com.mysql.cj.jdbc.Driver\r\n# 注意根据生产实际情况调整参数host和port\r\nstore.db.url=jdbc:mysql://192.168.159.102:3306/seata?rewriteBatchedStatements=true&useUnicode=true&characterEncoding=utf8&connectTimeout=1000&socketTimeout=3000&autoReconnect=true&useSSL=false\r\n# 数据库用户名\r\nstore.db.user=root\r\n# 用户名密码\r\nstore.db.password=123456\r\n# 微服务里配置与这里一致\r\nservice.vgroupMapping.order_tx_group=default\r\nservice.vgroupMapping.storage_tx_group=default\r\nservice.vgroupMapping.account_tx_group=default\r\n', 'b83ecb55bec2932626232c61c9302848', '2023-04-19 14:50:55', '2023-04-19 14:50:55', NULL, '192.168.159.1', '', '', NULL, NULL, NULL, 'properties', NULL, '');

-- ----------------------------
-- Table structure for config_info_aggr
-- ----------------------------
DROP TABLE IF EXISTS `config_info_aggr`;
CREATE TABLE `config_info_aggr`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `data_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'data_id',
  `group_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'group_id',
  `datum_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'datum_id',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '内容',
  `gmt_modified` datetime(0) NOT NULL COMMENT '修改时间',
  `app_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `tenant_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '租户字段',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_configinfoaggr_datagrouptenantdatum`(`data_id`, `group_id`, `tenant_id`, `datum_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '增加租户字段' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of config_info_aggr
-- ----------------------------

-- ----------------------------
-- Table structure for config_info_beta
-- ----------------------------
DROP TABLE IF EXISTS `config_info_beta`;
CREATE TABLE `config_info_beta`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `data_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'data_id',
  `group_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'group_id',
  `app_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'app_name',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'content',
  `beta_ips` varchar(1024) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'betaIps',
  `md5` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'md5',
  `gmt_create` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `gmt_modified` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  `src_user` text CHARACTER SET utf8 COLLATE utf8_bin NULL COMMENT 'source user',
  `src_ip` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'source ip',
  `tenant_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '租户字段',
  `encrypted_data_key` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '秘钥',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_configinfobeta_datagrouptenant`(`data_id`, `group_id`, `tenant_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = 'config_info_beta' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of config_info_beta
-- ----------------------------

-- ----------------------------
-- Table structure for config_info_tag
-- ----------------------------
DROP TABLE IF EXISTS `config_info_tag`;
CREATE TABLE `config_info_tag`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `data_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'data_id',
  `group_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'group_id',
  `tenant_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT 'tenant_id',
  `tag_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'tag_id',
  `app_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'app_name',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'content',
  `md5` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'md5',
  `gmt_create` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `gmt_modified` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  `src_user` text CHARACTER SET utf8 COLLATE utf8_bin NULL COMMENT 'source user',
  `src_ip` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'source ip',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_configinfotag_datagrouptenanttag`(`data_id`, `group_id`, `tenant_id`, `tag_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = 'config_info_tag' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of config_info_tag
-- ----------------------------

-- ----------------------------
-- Table structure for config_tags_relation
-- ----------------------------
DROP TABLE IF EXISTS `config_tags_relation`;
CREATE TABLE `config_tags_relation`  (
  `id` bigint(20) NOT NULL COMMENT 'id',
  `tag_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'tag_name',
  `tag_type` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'tag_type',
  `data_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'data_id',
  `group_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'group_id',
  `tenant_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT 'tenant_id',
  `nid` bigint(20) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`nid`) USING BTREE,
  UNIQUE INDEX `uk_configtagrelation_configidtag`(`id`, `tag_name`, `tag_type`) USING BTREE,
  INDEX `idx_tenant_id`(`tenant_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = 'config_tag_relation' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of config_tags_relation
-- ----------------------------

-- ----------------------------
-- Table structure for group_capacity
-- ----------------------------
DROP TABLE IF EXISTS `group_capacity`;
CREATE TABLE `group_capacity`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `group_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT 'Group ID，空字符表示整个集群',
  `quota` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '配额，0表示使用默认值',
  `usage` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '使用量',
  `max_size` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '单个配置大小上限，单位为字节，0表示使用默认值',
  `max_aggr_count` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '聚合子配置最大个数，，0表示使用默认值',
  `max_aggr_size` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '单个聚合数据的子配置大小上限，单位为字节，0表示使用默认值',
  `max_history_count` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '最大变更历史数量',
  `gmt_create` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `gmt_modified` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_group_id`(`group_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '集群、各Group容量信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of group_capacity
-- ----------------------------

-- ----------------------------
-- Table structure for his_config_info
-- ----------------------------
DROP TABLE IF EXISTS `his_config_info`;
CREATE TABLE `his_config_info`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nid` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `data_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `group_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `app_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'app_name',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `md5` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `gmt_create` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `gmt_modified` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `src_user` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `src_ip` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `op_type` char(10) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `tenant_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '租户字段',
  `encrypted_data_key` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '秘钥',
  PRIMARY KEY (`nid`) USING BTREE,
  INDEX `idx_gmt_create`(`gmt_create`) USING BTREE,
  INDEX `idx_gmt_modified`(`gmt_modified`) USING BTREE,
  INDEX `idx_did`(`data_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 25 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '多租户改造' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of his_config_info
-- ----------------------------
INSERT INTO `his_config_info` VALUES (0, 1, 'datasource.yml', 'dev', '', 'spring:\r\n  datasource:\r\n    driver-class-name: com.mysql.jdbc.Driver\r\n    username: root\r\n    password: 123456\r\n    url: jdbc:mysql://192.168.159.102:3306/gulimall_wms?useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=false&serverTimezone=GMT%2B8', '76e3e61f609d5692fdc09f35eabbb17a', '2023-04-04 06:29:59', '2023-04-05 14:11:21', NULL, '192.168.159.1', 'I', '8f0278b6-d6b7-412e-8c03-06dd8f71b72b', '');
INSERT INTO `his_config_info` VALUES (0, 2, 'mybatis.yml', 'dev', '', 'mybatis-plus:\r\n  mapper-location: classpath:/mapper/**/*.xml\r\n  global-config:\r\n    db-config:\r\n      id-type: auto\r\n', 'c872fb01f56a08a482a41f73fa3f6094', '2023-04-04 06:29:59', '2023-04-05 14:11:21', NULL, '192.168.159.1', 'I', '8f0278b6-d6b7-412e-8c03-06dd8f71b72b', '');
INSERT INTO `his_config_info` VALUES (0, 3, 'other.yml', 'dev', '', 'spring:\r\n  cloud:\r\n    nacos:\r\n      discovery:\r\n        server-addr: 127.0.0.1:8848\r\n  application:\r\n    name: gulimall-ware\r\n# 日志配置\r\nlogging:\r\n  level:\r\n    com.atguigu: debug\r\n    org.springframework: warn\r\n\r\nserver:\r\n  port: 11000', '874def5119d5c6da1adafdd03d1f9d27', '2023-04-04 06:29:59', '2023-04-05 14:11:21', NULL, '192.168.159.1', 'I', '8f0278b6-d6b7-412e-8c03-06dd8f71b72b', '');
INSERT INTO `his_config_info` VALUES (0, 4, 'gulimall-gateway.yml', 'DEFAULT_GROUP', '', 'spring:\r\n  application:\r\n    name: gulimall-gateway', '994cf84dbc3a8974a55026081dc689ae', '2023-04-04 06:32:30', '2023-04-05 14:13:51', NULL, '192.168.159.1', 'I', 'add4416d-3003-47a5-ac25-ee349cb42915', '');
INSERT INTO `his_config_info` VALUES (0, 5, 'datasource.yml', 'dev', '', 'spring:\r\n  datasource:\r\n    driver-class-name: com.mysql.jdbc.Driver\r\n    username: root\r\n    password: 123456\r\n    url: jdbc:mysql://192.168.159.102:3306/gulimall_pms?useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=false&serverTimezone=GMT%2B8\r\n  cloud:\r\n    nacos:\r\n      discovery:\r\n        server-addr: 127.0.0.1:8848\r\n  jackson:\r\n    date-format: yyy-MM-dd HH:mm:ss\r\n  application:\r\n    name: gulimall-product\r\n  #    一定要关缓存，否则会因为缓存原因导致热加载未更新\r\n  thymeleaf:\r\n    cache: false\r\n  redis:\r\n    host: 192.168.159.102\r\n    port: 6379', '019efe5c126a4bae9101eac3e5d5576e', '2023-04-04 06:32:52', '2023-04-05 14:14:14', NULL, '192.168.159.1', 'I', 'bb16d17e-fea5-4bd0-afcd-2aa199301315', '');
INSERT INTO `his_config_info` VALUES (0, 6, 'mybatis.yml', 'dev', '', 'mybatis-plus:\r\n  mapper-location: classpath:/mapper/**/*.xml\r\n  global-config:\r\n    db-config:\r\n      id-type: auto\r\n', 'c872fb01f56a08a482a41f73fa3f6094', '2023-04-04 06:32:52', '2023-04-05 14:14:14', NULL, '192.168.159.1', 'I', 'bb16d17e-fea5-4bd0-afcd-2aa199301315', '');
INSERT INTO `his_config_info` VALUES (0, 7, 'other.yml', 'dev', '', 'spring:\r\n  cloud:\r\n    nacos:\r\n      discovery:\r\n        server-addr: 127.0.0.1:8848\r\n  application:\r\n    name: gulimall-product\r\n# 日志配置\r\nlogging:\r\n  level:\r\n    com.atguigu: debug\r\n    org.springframework: warn\r\n\r\nserver:\r\n  port: 10010', '749510bd8e9356440e5b6788543de1f6', '2023-04-04 06:32:52', '2023-04-05 14:14:14', NULL, '192.168.159.1', 'I', 'bb16d17e-fea5-4bd0-afcd-2aa199301315', '');
INSERT INTO `his_config_info` VALUES (0, 8, 'datasource.yml', 'dev', '', '# 数据源配置\r\nspring:\r\n  datasource:\r\n    type: com.alibaba.druid.pool.DruidDataSource\r\n    driverClassName: com.mysql.cj.jdbc.Driver\r\n    druid:\r\n      # 主库数据源\r\n      master:\r\n        url: jdbc:mysql://192.168.159.102:3306/gulimall_admin?useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=false&serverTimezone=GMT%2B8\r\n        username: root\r\n        password: 123456\r\n      # 从库数据源\r\n      slave:\r\n        # 从数据源开关/默认关闭\r\n        enabled: false\r\n        url:\r\n        username:\r\n        password:\r\n      # 初始连接数\r\n      initialSize: 5\r\n      # 最小连接池数量\r\n      minIdle: 10\r\n      # 最大连接池数量\r\n      maxActive: 20\r\n      # 配置获取连接等待超时的时间\r\n      maxWait: 60000\r\n      # 配置间隔多久才进行一次检测，检测需要关闭的空闲连接，单位是毫秒\r\n      timeBetweenEvictionRunsMillis: 60000\r\n      # 配置一个连接在池中最小生存的时间，单位是毫秒\r\n      minEvictableIdleTimeMillis: 300000\r\n      # 配置一个连接在池中最大生存的时间，单位是毫秒\r\n      maxEvictableIdleTimeMillis: 900000\r\n      # 配置检测连接是否有效\r\n      validationQuery: SELECT 1 FROM DUAL\r\n      testWhileIdle: true\r\n      testOnBorrow: false\r\n      testOnReturn: false\r\n      webStatFilter:\r\n        enabled: true\r\n      statViewServlet:\r\n        enabled: true\r\n        # 设置白名单，不填则允许所有访问\r\n        allow:\r\n        url-pattern: /druid/*\r\n        # 控制台管理用户名和密码\r\n        login-username: ruoyi\r\n        login-password: 123456\r\n      filter:\r\n        stat:\r\n          enabled: true\r\n          # 慢SQL记录\r\n          log-slow-sql: true\r\n          slow-sql-millis: 1000\r\n          merge-sql: true\r\n        wall:\r\n          config:\r\n            multi-statement-allow: true', 'b802bd44e7a3e05236a8791c321e0db3', '2023-04-04 06:33:10', '2023-04-05 14:14:31', NULL, '192.168.159.1', 'I', '36576576-18d5-4a83-81c9-b8c0f4fe385b', '');
INSERT INTO `his_config_info` VALUES (0, 9, 'mybatis.yml', 'dev', '', '# MyBatis配置\r\nmybatis:\r\n    # 搜索指定包别名\r\n    typeAliasesPackage: com.ruoyi.**.domain\r\n    # 配置mapper的扫描，找到所有的mapper.xml映射文件\r\n    mapperLocations: classpath*:mapper/**/*Mapper.xml\r\n    # 加载全局的配置文件\r\n    configLocation: classpath:mybatis/mybatis-config.xml\r\n\r\n# PageHelper分页插件\r\npagehelper:\r\n  helperDialect: mysql\r\n  supportMethodsArguments: true\r\n  params: count=countSql', '0fc9ae5a0fdc6d825bfbdbf6754d8f00', '2023-04-04 06:33:10', '2023-04-05 14:14:31', NULL, '192.168.159.1', 'I', '36576576-18d5-4a83-81c9-b8c0f4fe385b', '');
INSERT INTO `his_config_info` VALUES (0, 10, 'other.yml', 'dev', '', '# 项目相关配置\r\nruoyi:\r\n  # 名称\r\n  name: RuoYi\r\n  # 版本\r\n  version: 3.8.1\r\n  # 版权年份\r\n  copyrightYear: 2022\r\n  # 实例演示开关\r\n  demoEnabled: true\r\n  # 文件路径 示例（ Windows配置D:/ruoyi/uploadPath，Linux配置 /home/ruoyi/uploadPath）\r\n  profile: D:/ruoyi/uploadPath\r\n  # 获取ip地址开关\r\n  addressEnabled: false\r\n  # 验证码类型 math 数组计算 char 字符验证\r\n  captchaType: math\r\n  fastdfs: \r\n    path: /temp/\r\n\r\n# 开发环境配置\r\nserver:\r\n  # 服务器的HTTP端口，默认为8080\r\n  port: 8081\r\n  servlet:\r\n    # 应用的访问路径\r\n    context-path: /ruoyi\r\n  tomcat:\r\n    # tomcat的URI编码\r\n    uri-encoding: UTF-8\r\n    # 连接数满后的排队数，默认为100\r\n    accept-count: 1000\r\n    threads:\r\n      # tomcat最大线程数，默认为200\r\n      max: 800\r\n      # Tomcat启动初始化的线程数，默认值10\r\n      min-spare: 100\r\n\r\n# 日志配置\r\nlogging:\r\n  level:\r\n    com.ruoyi: debug\r\n    org.springframework: warn\r\n\r\n# Spring配置\r\nspring:\r\n  # 资源信息\r\n  messages:\r\n    # 国际化资源文件路径\r\n    basename: i18n/messages\r\n  # 文件上传\r\n  servlet:\r\n     multipart:\r\n       # 单个文件大小\r\n       max-file-size:  10MB\r\n       # 设置总上传的文件大小\r\n       max-request-size:  20MB\r\n  # 服务模块\r\n  devtools:\r\n    restart:\r\n      # 热部署开关\r\n      enabled: true\r\n  # redis 配置\r\n  redis:\r\n    # 地址\r\n    host: 192.168.159.102\r\n    # 端口，默认为6379\r\n    port: 6379\r\n    # 数据库索引\r\n    database: 0\r\n    # 密码\r\n    password:\r\n    # 连接超时时间\r\n    timeout: 10s\r\n    lettuce:\r\n      pool:\r\n        # 连接池中的最小空闲连接\r\n        min-idle: 0\r\n        # 连接池中的最大空闲连接\r\n        max-idle: 8\r\n        # 连接池的最大数据库连接数\r\n        max-active: 8\r\n        # #连接池最大阻塞等待时间（使用负值表示没有限制）\r\n        max-wait: -1ms\r\n        #注册中心\r\n  cloud:\r\n    nacos:\r\n      discovery:\r\n        server-addr: 127.0.0.1:8848\r\n  application:\r\n    name: gulimall-ruoyi\r\n  mvc:\r\n    pathmatch:\r\n      matching-strategy: ant_path_matcher\r\n\r\n# token配置\r\ntoken:\r\n    # 令牌自定义标识\r\n    header: Authorization\r\n    # 令牌密钥\r\n    secret: abcdefghijklmnopqrstuvwxyz\r\n    # 令牌有效期（默认30分钟）\r\n    expireTime: 30\r\n\r\n# Swagger配置\r\nswagger:\r\n  # 是否开启swagger\r\n  enabled: true\r\n  # 请求前缀\r\n  pathMapping: /dev-api\r\n\r\n# 防止XSS攻击\r\nxss:\r\n  # 过滤开关\r\n  enabled: true\r\n  # 排除链接（多个用逗号分隔）\r\n  excludes: /system/notice\r\n  # 匹配链接\r\n  urlPatterns: /system/*,/monitor/*,/tool/*\r\n', 'd026098f848fe75330b410f1107b3527', '2023-04-04 06:33:10', '2023-04-05 14:14:31', NULL, '192.168.159.1', 'I', '36576576-18d5-4a83-81c9-b8c0f4fe385b', '');
INSERT INTO `his_config_info` VALUES (0, 11, 'datasource.yml', 'dev', '', 'spring:\r\n  datasource:\r\n    driver-class-name: com.mysql.jdbc.Driver\r\n    username: root\r\n    password: 123456\r\n    url: jdbc:mysql://192.168.159.102:3306/gulimall_ums?useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=false&serverTimezone=GMT%2B8', '51505ed02f20934d3d6cadee10f69002', '2023-04-04 06:33:36', '2023-04-05 14:14:57', NULL, '192.168.159.1', 'I', '832d69fc-3d7f-4d2b-803c-61d0f52ec6f3', '');
INSERT INTO `his_config_info` VALUES (0, 12, 'mybatis.yml', 'dev', '', 'mybatis-plus:\r\n  mapper-location: classpath:/mapper/**/*.xml\r\n  global-config:\r\n    db-config:\r\n      id-type: auto\r\n', 'c872fb01f56a08a482a41f73fa3f6094', '2023-04-04 06:33:36', '2023-04-05 14:14:57', NULL, '192.168.159.1', 'I', '832d69fc-3d7f-4d2b-803c-61d0f52ec6f3', '');
INSERT INTO `his_config_info` VALUES (0, 13, 'other.yml', 'dev', '', 'spring:\r\n  cloud:\r\n    nacos:\r\n      discovery:\r\n        server-addr: 127.0.0.1:8848\r\n  application:\r\n    name: gulimall-member\r\n# 日志配置\r\nlogging:\r\n  level:\r\n    com.atguigu: debug\r\n    org.springframework: warn\r\n\r\nserver:\r\n  port: 8000', '8ae41f88bbf8dcd4474d9b8c4a51ddb9', '2023-04-04 06:33:36', '2023-04-05 14:14:57', NULL, '192.168.159.1', 'I', '832d69fc-3d7f-4d2b-803c-61d0f52ec6f3', '');
INSERT INTO `his_config_info` VALUES (0, 14, 'gulimall-coupon.properties', 'DEFAULT_GROUP', '', 'coupon.user.name = huangwei\r\ncoupon.user.age = 23', '20d2c4eef3c148e7c277d5069e90a9b0', '2023-04-04 06:33:50', '2023-04-05 14:15:11', NULL, '192.168.159.1', 'I', '09e41474-0479-403e-a8b5-529cd8f1f9af', '');
INSERT INTO `his_config_info` VALUES (0, 15, 'datasource.yml', 'dev', '', 'spring:\r\n  datasource:\r\n    driver-class-name: com.mysql.jdbc.Driver\r\n    username: root\r\n    password: 123456\r\n    url: jdbc:mysql://192.168.159.102:3306/gulimall_sms?useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=false&serverTimezone=GMT%2B8\r\n    ', 'bfd79d5b64729d80568701a0c39d1e27', '2023-04-04 06:33:50', '2023-04-05 14:15:11', NULL, '192.168.159.1', 'I', '09e41474-0479-403e-a8b5-529cd8f1f9af', '');
INSERT INTO `his_config_info` VALUES (0, 16, 'mybatis.yml', 'dev', '', 'mybatis-plus:\r\n  mapper-location: classpath:/mapper/**/*.xml\r\n  global-config:\r\n    db-config:\r\n      id-type: auto\r\n', 'c872fb01f56a08a482a41f73fa3f6094', '2023-04-04 06:33:50', '2023-04-05 14:15:11', NULL, '192.168.159.1', 'I', '09e41474-0479-403e-a8b5-529cd8f1f9af', '');
INSERT INTO `his_config_info` VALUES (0, 17, 'other.yml', 'dev', '', 'spring:\r\n  cloud:\r\n    nacos:\r\n      discovery:\r\n        server-addr: 127.0.0.1:8848\r\n  application:\r\n    name: gulimall-coupon\r\n    \r\nserver:\r\n  port: 7000\r\n#        下面这种方式不知道配置中心可不可以正常启动\r\n#      config:\r\n#        server-addr: 127.0.0.1:8848', '1a4b6a3ec2299afafe23c4522b7ea6d6', '2023-04-04 06:33:50', '2023-04-05 14:15:11', NULL, '192.168.159.1', 'I', '09e41474-0479-403e-a8b5-529cd8f1f9af', '');
INSERT INTO `his_config_info` VALUES (0, 18, 'service.vgroupMapping.my_test_tx_group', 'DEFAULT_GROUP', '', '1', 'c4ca4238a0b923820dcc509a6f75849b', '2023-04-18 23:25:49', '2023-04-18 15:29:19', NULL, '192.168.159.1', 'I', 'db2be525-d6a2-4b87-9d14-22a6b47d7901', '');
INSERT INTO `his_config_info` VALUES (18, 19, 'service.vgroupMapping.my_test_tx_group', 'DEFAULT_GROUP', '', '1', 'c4ca4238a0b923820dcc509a6f75849b', '2023-04-18 23:26:07', '2023-04-18 15:29:37', NULL, '192.168.159.1', 'D', 'db2be525-d6a2-4b87-9d14-22a6b47d7901', '');
INSERT INTO `his_config_info` VALUES (0, 20, 'service.vgroupMapping.my_test_tx_group', 'SEATA_GROUP', '', '1', 'c4ca4238a0b923820dcc509a6f75849b', '2023-04-18 23:26:18', '2023-04-18 15:29:48', NULL, '192.168.159.1', 'I', 'db2be525-d6a2-4b87-9d14-22a6b47d7901', '');
INSERT INTO `his_config_info` VALUES (19, 21, 'service.vgroupMapping.my_test_tx_group', 'SEATA_GROUP', '', '1', 'c4ca4238a0b923820dcc509a6f75849b', '2023-04-18 23:32:31', '2023-04-18 15:36:00', NULL, '192.168.159.1', 'D', 'db2be525-d6a2-4b87-9d14-22a6b47d7901', '');
INSERT INTO `his_config_info` VALUES (0, 22, 'seataServer.properties', 'public', '', '1', 'c4ca4238a0b923820dcc509a6f75849b', '2023-04-19 00:03:08', '2023-04-19 14:39:30', NULL, '192.168.159.1', 'I', 'f905cd15-48f3-4727-8fd1-70d261f41462', '');
INSERT INTO `his_config_info` VALUES (20, 23, 'seataServer.properties', 'public', '', '1', 'c4ca4238a0b923820dcc509a6f75849b', '2023-04-19 00:09:37', '2023-04-19 14:46:00', NULL, '192.168.159.1', 'D', 'f905cd15-48f3-4727-8fd1-70d261f41462', '');
INSERT INTO `his_config_info` VALUES (0, 24, 'seataServer.properties', 'SEATA_GROUP', '', '# 存储模式\r\nstore.mode=db\r\n \r\nstore.db.datasource=druid\r\nstore.db.dbType=mysql\r\n# 需要根据mysql的版本调整driverClassName\r\n# mysql8及以上版本对应的driver：com.mysql.cj.jdbc.Driver\r\n# mysql8以下版本的driver：com.mysql.jdbc.Driver\r\nstore.db.driverClassName=com.mysql.cj.jdbc.Driver\r\n# 注意根据生产实际情况调整参数host和port\r\nstore.db.url=jdbc:mysql://192.168.159.102:3306/seata?rewriteBatchedStatements=true&useUnicode=true&characterEncoding=utf8&connectTimeout=1000&socketTimeout=3000&autoReconnect=true&useSSL=false\r\n# 数据库用户名\r\nstore.db.user=root\r\n# 用户名密码\r\nstore.db.password=123456\r\n# 微服务里配置与这里一致\r\nservice.vgroupMapping.order_tx_group=default\r\nservice.vgroupMapping.storage_tx_group=default\r\nservice.vgroupMapping.account_tx_group=default\r\n', 'b83ecb55bec2932626232c61c9302848', '2023-04-19 00:14:33', '2023-04-19 14:50:55', NULL, '192.168.159.1', 'I', '', '');

-- ----------------------------
-- Table structure for permissions
-- ----------------------------
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions`  (
  `role` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `resource` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `action` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  UNIQUE INDEX `uk_role_permission`(`role`, `resource`, `action`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of permissions
-- ----------------------------

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles`  (
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `role` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  UNIQUE INDEX `idx_user_role`(`username`, `role`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES ('nacos', 'ROLE_ADMIN');

-- ----------------------------
-- Table structure for tenant_capacity
-- ----------------------------
DROP TABLE IF EXISTS `tenant_capacity`;
CREATE TABLE `tenant_capacity`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `tenant_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT 'Tenant ID',
  `quota` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '配额，0表示使用默认值',
  `usage` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '使用量',
  `max_size` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '单个配置大小上限，单位为字节，0表示使用默认值',
  `max_aggr_count` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '聚合子配置最大个数',
  `max_aggr_size` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '单个聚合数据的子配置大小上限，单位为字节，0表示使用默认值',
  `max_history_count` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '最大变更历史数量',
  `gmt_create` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `gmt_modified` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_tenant_id`(`tenant_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '租户容量信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tenant_capacity
-- ----------------------------

-- ----------------------------
-- Table structure for tenant_info
-- ----------------------------
DROP TABLE IF EXISTS `tenant_info`;
CREATE TABLE `tenant_info`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `kp` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'kp',
  `tenant_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT 'tenant_id',
  `tenant_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT 'tenant_name',
  `tenant_desc` varchar(256) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'tenant_desc',
  `create_source` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'create_source',
  `gmt_create` bigint(20) NOT NULL COMMENT '创建时间',
  `gmt_modified` bigint(20) NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_tenant_info_kptenantid`(`kp`, `tenant_id`) USING BTREE,
  INDEX `idx_tenant_id`(`tenant_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = 'tenant_info' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tenant_info
-- ----------------------------
INSERT INTO `tenant_info` VALUES (1, '1', '8f0278b6-d6b7-412e-8c03-06dd8f71b72b', 'ware', '仓库', 'nacos', 1680703869589, 1680703869589);
INSERT INTO `tenant_info` VALUES (2, '1', 'add4416d-3003-47a5-ac25-ee349cb42915', 'gateway', '网关', 'nacos', 1680703918005, 1680703918005);
INSERT INTO `tenant_info` VALUES (3, '1', '4a921290-2a7a-4bfb-82e2-6a8981628ffd', 'order', '订单服务', 'nacos', 1680703930010, 1680703930010);
INSERT INTO `tenant_info` VALUES (4, '1', '6e260638-8ffb-495f-ad58-5814c74fcbd1', 'thiry-party', '第三方服务', 'nacos', 1680703942348, 1680703942348);
INSERT INTO `tenant_info` VALUES (5, '1', 'bb16d17e-fea5-4bd0-afcd-2aa199301315', 'product', '商品首页', 'nacos', 1680703958784, 1680703958784);
INSERT INTO `tenant_info` VALUES (6, '1', '36576576-18d5-4a83-81c9-b8c0f4fe385b', 'admin', '管理员后台', 'nacos', 1680703967847, 1680703967847);
INSERT INTO `tenant_info` VALUES (7, '1', '832d69fc-3d7f-4d2b-803c-61d0f52ec6f3', 'member', '用户系统', 'nacos', 1680703977730, 1680703977730);
INSERT INTO `tenant_info` VALUES (8, '1', '09e41474-0479-403e-a8b5-529cd8f1f9af', 'coupon', '积分', 'nacos', 1680703988957, 1680703988957);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  PRIMARY KEY (`username`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('nacos', '$2a$10$EuWPZHzz32dJN7jexM34MOeYirDdFAZm2kuWj7VEOJhhZkDrxfvUu', 1);

SET FOREIGN_KEY_CHECKS = 1;
