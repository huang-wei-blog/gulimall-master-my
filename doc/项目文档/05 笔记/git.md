# 基础

## 初始化git

git init

提交

git add xx.xx

git commit -m '注释'

## 查看文件的状态

git status / git status -s

表示工作空间非常整洁：

```git
On branch master
nothing to commit, working directory clean
```

暂存状态

```git
只要在 Changes to be committed 这行下面的，就说明是已暂存状态。
```

暂存发生改变

```git
文件 CONTRIBUTING.md 出现在 Changes not staged for commit 这行下面，说明已跟踪文件的内容发
生了变化，但还没有放到暂存区。 要暂存这次更新，需要运行 git add 命令。
```

如果一个文件发生修改，出现在暂存区和非暂存区，如果这个时候提交，git提交的是你最后一次提交的版本，而非你修改的版本。这个时候你需要添加到这个文件到暂存区，然后在提交。

```
 M README
MM Rakefile
A lib/git.rb
M lib/simplegit.rb
?? LICENSE.txt
新加的未跟踪的文件有??标记
新增到暂存区中的文件前面有A标记
修改过的文件前面有M的标记
两个M，右边的M表示文件修改了，还没放到暂存区，出现在靠左边的M表示文件修改了，并放入暂存区
```

## 忽略文件

```
$ cat .gitignore
*.[oa]
*~
• 所有空行或者以 ＃ 开头的行都会被 Git 忽略。
• 可以使用标准的 glob 模式匹配。
• 匹配模式可以以（/）开头防止递归。 
• 匹配模式可以以（/）结尾指定目录。
• 要忽略指定模式以外的文件或目录，可以在模式前加上惊叹号（!）取反。 所谓的 glob 模式是指 shell 所使用的简化了的正则表达式。 星号（*）匹配零个或多个任意字符；[abc] 匹配任何一个列在方括号中的字符（这个例子要么匹配一个 a，要么匹配一个 b，要么匹配一个 c）；问号（?
）只匹配一个任意字符；如果在方括号中使用短划线分隔两个字符，表示所有在这两个字符范围内的都可以匹配 （比如 [0-9] 表示匹配所有 0 到 9 的数字）。 使用两个星号（*) 表示匹配任意中间目录，比如 a/**/z 可以匹
配 a/z , a/b/z 或 a/b/c/z 等。

https://github.com/github/gitignore
```

## 查看以暂存和未暂存的修改

查看具体修改了那些地方

git diff(只查看尚未暂存的改动，而不是上次提交以来所做的改动)

查看以暂存的将要添加到下次提交里的内容，可以用git diff --cached 或者git diff --staged

跳过暂存区，直接提交

git commit -a -m '注释'

## 移除文件

git rm 完成，从暂存区移除，然后提交。

强制删除加上-f

移除暂存区的文件
git rm --cached log/\*.log

移动文件 git mv

## 查看提交历史

## 常用选项

```
-p 按补丁格式显示每个更新之间的差异。
--stat 显示每次更新的文件修改统计信息。
--shortstat 只显示 --stat 中最后的行数修改添加移除统计。
--name-only 仅在提交信息后显示已修改的文件清单。
--name-status 显示新增、修改、删除的文件清单。
--abbrev-commit 仅显示 SHA-1 的前几个字符，而非所有的 40 个字符。
--relative-date 使用较短的相对时间显示（比如，“2 weeks ago”）。
--graph 显示 ASCII 图形表示的分支合并历史。
--pretty 使用其他格式显示历史提交信息。可用的选项包括 oneline，short，full，fuller 和
format（后跟指定格式）。
```

git log

查看近期两次提交

git log -2

查看近两周的提交

git log --since=2.weeks

```
-(n) 仅显示最近的 n 条提交
--since, --after 仅显示指定时间之后的提交。
--until, --before 仅显示指定时间之前的提交。
--author 仅显示指定作者相关的提交。
--committer 仅显示指定提交者相关的提交。
--grep 仅显示含指定关键字的提交
-S 仅显示添加或移除了某个关键字的提交
```

查看每次提交内容的差异

git log -p

查看每次提交的简略统计
git log --stat

将提交信息展示在第一行

git log --pretty=oneline

将提交信息展示在第一行，将提交到具体的git仓库展示在第二行

git log --pretty=short

查看提交信息、提交的仓库信息、操作信息

git log  --pretty=full

查看提交的信息、提交的仓库、提交的日期、操作信息

git log --pretty=fuller

定制输出格式，查看文件的作者、和作者修订的日期、以及提交说明

git log --pretty=format : "%h - %an,%ar : %s"

```
%H 提交对象（commit）的完整哈希字串
%h 提交对象的简短哈希字串
%T 树对象（tree）的完整哈希字串
%t 树对象的简短哈希字串
%P 父对象（parent）的完整哈希字串
%p 父对象的简短哈希字串
%an 作者（author）的名字
%ae 作者的电子邮件地址
%ad 作者修订日期（可以用 --date= 选项定制格式）
%ar 作者修订日期，按多久以前的方式显示
%cn 提交者（committer）的名字
%ce 提交者的电子邮件地址
%cd 提交日期
%cr 提交日期，按多久以前的方式显示
%s 提交说明
```

## 撤销操作

撤销提交

git commit --amend

取消暂存

git reset HEAD xx.xx(该操作可能会导致丢失工作目录所有当前的进度，git reset不加选项的调用并不危险)

撤销文件的修改

git checkout -- xx.xx（该操作会丢失文件中的任何修改）

## 远程仓库

查看远程仓库

git remote

查看需要读写的远程仓库

git remote -v

添加运程仓库(xxx 为url的简写，可视为一个变量)

git remote add xxx URL

修改远程仓库url的简写(这个同时也会修改你远程分支的名字)

git remote rename xxx xxx

删除镜像/分支

git remote rm xxx

拉去仓库中有，但你没有的信息

git fetch xxx

抓取并合并分支

git pull

推送到远程仓库(只有当你有服务器的写入权限，并且之前没有人推送过的时候，这条命令才生效，如果有人推送，你必须先把他们的拉去下拉合并工作才能继续推送)

语法：git push [remote-name] [branch-name]

git push orgin master

查看远程仓库的更多信息

git remote show origin

## 打标签

列出标签

git tag

查看特定的标签
git tag -l 'v1.4*'

查看标签与对应的提交信息

git show v1.4

### 创建标签

轻量级标签（不会改变分支，通常用于临时的）

git tag v1.4-lw

创建附注标签(包含打标签者的名字、电子邮件、日期信息)

git tag -a  v1.4 'my version 1.4'

补标签(需要在命令的末尾指定提交的校验和（或部分校验和）)

$ git tag -a v1.2 9fceb02

共享标签

git push origin v1.5

将所有不在远程仓库中的标签一次性传输

git push origin --tags

删除本地标签(不会删除远程仓库)

git tag -d xxx

git push origin :refs/tags/v1.4-lw （删除后更新远程仓库，远程仓库的标签才会删除）

检出标签

git checkout -b version2 v2.0.0

## 别名

git config --global alias.last 'log -1 HEAD' 等价与 git log -1

# 分支

创建分支

git branch testing

查看各个分支当前所指的对象

git log --oneline --decorate

切换分支(切换到一个较为旧的分支，你的工作目录会恢复到该分支最后一次提交的样子)

git checkout testing

输出你的提交历史、各个分支的指向以及项目的分支分叉情况

 git log --oneline --decorate --graph --all

## 现实开发的流程

1. 开发某个网站。 

2. 为实现某个新的需求，创建一个分支。 

3. 在这个分支上开展工作。 
   
   正在此时，你突然接到一个电话说有个很严重的问题需要紧急修补。 你将按照如下方式来处理： 

4. 切换到你的线上分支（production branch）。 

5. 为这个紧急任务新建一个分支，并在其中修复它。 

6. 在测试通过之后，切换回线上分支，然后合并这个修补分支，最后将改动推送到线上分支。 

7. 切换回你最初工作的分支上，继续工作。 

建议一个紧急的分支

git checkout -b hotfix

合并分支

git checkout master 

git merge hotfix

删除分支
git branch -d hotfix

得到分支列表
git branch（有*号的代表HEAD指针所指向的分支）

查看每个分支最后一次提交

git branch -v

查看那分支已经合并当前分支（合并后的分支可以删除，因为可以合并到另一个分支，所以删除也不会有什么影响）

git branch --merged

查看所有包含未合并的分支

git branch --no--merged

在某某分支上一起工作
git push origin xxx

每次推送都需要输入密码，你可以这样设置它避免这样

设置一个 “credential cache”

git config --global credential.helper cache

如果自己想要在serverfix分支上工作，可以建立远程跟踪

git checkout -b serverfix origin/serverfix

删除远程分支
git push origin --delete serverfix

git dev分支同步主分支代码
================

1.git checkout master #切换到master分支上
2.git pull #将代码都pull到本地
3.git checkout dev #再次切换到dev分支上
4.git merge master #将master上的内容合并到dev
5.git push #将本地内容在push到远程仓库dev分支
强制推送
git push -u origin master -f
