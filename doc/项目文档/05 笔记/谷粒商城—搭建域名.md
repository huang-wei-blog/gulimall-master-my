# 开发环境

什么是正向代理？什么是反向代理？

正向与反向是对于我们自己的电脑来说，如果它帮助我们直接访问对方服务，隐藏客户端信息，这就是正向代理。

反向代理，屏蔽内网服务器信息，负载均衡访问。

**备注：回炉计算机网络！！！！！！**

# Nginx+网关+openFeign的逻辑

要实现的逻辑：本机浏览器请求gulimall.com，通过配置hosts文件之后，那么当你在浏览器中输入gulimall.com的时候，相当于域名解析DNS服务解析得到ip 192.168.56.10，也就是并不是访问java服务，而是先去找nginx。什么意思呢？是说如果某一天项目上线了，gulimall.com应该是nginx的ip，用户访问的都是nginx请求到了nginx之后，如果是静态资源/static/直接在nginx服务器中找到静态资源直接返回。
如果不是静态资源/（他配置在/static/的后面所以才优先级低），nginx把他upstream转交给另外一个ip 192.168.56.1:88这个ip端口是网关gateway。
（在upstream的过程中要注意配置proxy_set_header Host $host;）
到达网关之后，通过url信息断言判断应该转发给nacos中的哪个微服务（在给nacos之前也可以重写url），这样就得到了响应而对于openFeign，因为在服务中注册了nacos的ip，所以他并不经过nginx。

# Nginx配置文件

## nginx.conf

全局块：配置影响nginx全局的指令。如：用户组，nginx进程pid存放路径，日志存放路径，配置文件引入，允许生成worker process故障等
events块：配置影响 Nginx 服务器与用户的网络连接，常用的设置包括是否开启对多 work process下的网络连接进行序列化，是否允许同时接收多个网络连接，选取哪种事件驱动模型来处理连接请求，每个 word process 可以同时支持的最大连接数等。

## http块

http全局块：配置的指令包括文件引入、MIME-TYPE 定义、日志自定义、连接超时时间、单链接请求数上限等。错误页面等
server块：这块和虚拟主机有密切关系，虚拟主机从用户角度看，和一台独立的硬件主机是完全一样的。每个 http 块可以包括多个 server 块，而每个 server 块就相当于一个虚拟主机。
location1：配置请求的路由，以及各种页面的处理情况
location2

## Nginx+网关配置

1、修改主机hosts，映射gulimall.com到192.168.56.10。关闭防火墙

2、修改nginx/conf/nginx.conf，将upstream映射到我们的网关服务

    upstream gulimall{
        # 88是网关
        server 192.168.56.1:88;
    }

3、修改`nginx/conf/conf.d/gulimall.conf`，接收到gulimall.com的访问后，如果是/，转交给指定的upstream，由于nginx的转发会丢失`host`头，造成网关不知道原host，所以我们添加头信息

```
  location / {
        proxy_pass http://gulimall;
        proxy_set_header Host $host;
    }

```

4、配置gateway为服务器，将域名为`**.gulimall.com`转发至商品服务。配置的时候注意 网关优先匹配的原则，所以要把这个配置放到后面

```
    - id: gulimall_host_route
          uri: lb://gulimall-product
          predicates:
            - Host=**.gulimall.com,gulimall.com

```

## Nginx 配置conf.d/gulimall.conf

监听来自gulimall:80的请求，对于以/static开头的请求，就是找 /usr/share/nginx/html这个相对路径。为什么找那个？因为我们映射了docker外面的/mydata/data/nginx/html某一列到这个目录，所以在docker中就是去这找静态资源其他的请求，转发到http://gulimall 这个upstream ，并且由于nginx的转发会丢失host头（host头是HTTP1.1开始新增的请求头），造成网关不知道原host，所以我们添加头信息

## nginx.conf

在这里最重要的是这个再转给网关的配置

````
这个地方因为可能把后面视频的内容也挪过来了所以写的比较乱，也懒得改了，总之就是分为/static拦截和/拦截，将/拦截转发到upstream gulimall即可，转发时代上请求头，Nginx的原理其实就是 NIO-select/read+线程池 ，很多中间件/框架的原理都是这个
````

```
user  nginx;
worker_processes  1;

error_log  /var/log/nginx/error.log warn;
pid        /var/run/nginx.pid;


events {
    worker_connections  1024;
}


http {
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;

    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;
	
	upstream gulimall{
        # 88是网关
        server 192.168.56.1:88;
    }
    
    sendfile        on;
    #tcp_nopush     on;

    keepalive_timeout  65;

    include /etc/nginx/conf.d/*.conf;  # 包含了哪些配置文件
}

```

gulimall.conf

```
server {
    listen       80;
    server_name gulimall.com;
	
	
    location /static/ {
        root   /usr/share/nginx/html;
    }
    
	location / {
        proxy_set_header Host $host;
        proxy_pass http://gulimall;

    }
}

```



# 搭建域名

原理：在windos里面我们指定了gulimall.com映射的是虚拟机IP，浏览器访问gulimall.com，会来到虚拟机里面的nginx监听的80端口，nginx又帮我代理到本机

# 上线环境